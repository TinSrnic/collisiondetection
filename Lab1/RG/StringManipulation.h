#pragma once
#include <vector>
#include <string>

std::vector<std::string> SplitString(const std::string& t_string, const char t_delimiter) {
	std::vector<std::string> res = std::vector<std::string>();

	unsigned int lastPos = 0;
	unsigned int count = 0;
	unsigned int i;

	for (i = 0; i < t_string.size(); ++i) {
		if (t_string[i] == t_delimiter) {
			res.push_back(t_string.substr(lastPos, count));
			lastPos = i + 1;
			count = 0;
		} else {
			count++;
		}
	}

	res.push_back(t_string.substr(lastPos, i));
	return res;
}
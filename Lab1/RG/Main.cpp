#include <stdio.h>
#include <iostream>
#include <string>
#include <GLUT\freeglut.h>
#include <GLM\mat3x3.hpp>
#include <GLM\vec3.hpp>
#include <GLM\gtc\matrix_transform.hpp>
#define _USE_MATH_DEFINES
#include <math.h>

#include "OBJLoader.h"
#include "BSpline.h"

float maxTime = 10.0f;
bool idle = true;

BSpline spline = BSpline("spiralTrue");

GLfloat* vertices = nullptr;
GLfloat* normals = nullptr;
int count = 0;

glm::vec3 color = glm::vec3(135, 206, 250);
glm::vec3 ociste;
glm::vec3 light = glm::vec3(-1.0f, 0.0f, -1.0f);

//*********************************************************************************
//	Pokazivac na glavni prozor i pocetna velicina.
//*********************************************************************************
GLuint window;
GLuint width = 800, height = 600;

//*********************************************************************************
//	Function Prototypes.
//*********************************************************************************
void myDisplay();
void myReshape(int width, int height);
void myMouse(int button, int state, int x, int y);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);
void myObject();
void myIdle();

//*********************************************************************************
//	Glavni program.
//*********************************************************************************
int main(int argc, char** argv) {

	ociste = glm::vec3(0.0f, 0.0f, 5.0f);

	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutInitWindowSize(width, height);
	glutInitWindowPosition(100, 100);
	glutInit(&argc, argv);

	window = glutCreateWindow("Tijelo");
	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	glutMouseFunc(myMouse);
	glutKeyboardFunc(myKeyboard);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	LoadOBJ("bull", vertices, normals, count);

	glutMainLoop();

	return 0;
}

void myIdle() {
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);		         // boja pozadine - bijela
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	myObject();
	glutSwapBuffers();      // iscrtavanje iz dvostrukog spemnika (umjesto glFlush)
}

//*********************************************************************************
//	Osvjezavanje prikaza. (nakon preklapanja prozora) 
//*********************************************************************************
void myDisplay() {
	//printf("Pozvan myDisplay()\n");
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);		         // boja pozadine - bijela
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	myObject();
	glutSwapBuffers();      // iscrtavanje iz dvostrukog spemnika (umjesto glFlush)
}

//*********************************************************************************
//	Promjena velicine prozora.
//*********************************************************************************
void myReshape(int w, int h) {
	width = w; height = h;
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);        // aktivirana matrica projekcije
	glLoadIdentity();
	gluPerspective(45.0, (float)width / height, 0.5, 8.0); // kut pogleda, x/y, prednja i straznja ravnina odsjecanja
	gluLookAt(ociste.x, ociste.y, ociste.z, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);	// ociste x,y,z; glediste x,y,z; up vektor x,y,z
	glMatrixMode(GL_MODELVIEW);         // aktivirana matrica modela
}

void updatePerspective() {
	glMatrixMode(GL_PROJECTION);        // aktivirana matrica projekcije
	glLoadIdentity();
	gluPerspective(45.0, (float)width / height, 0.5, 8.0); // kut pogleda, x/y, prednja i straznja ravnina odsjecanja
	gluLookAt(ociste.x, ociste.y, ociste.z, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);	// ociste x,y,z; glediste x,y,z; up vektor x,y,z
	glMatrixMode(GL_MODELVIEW);         // aktivirana matrica modela
}

void myObject() {
	//	glutWireCube (1.0);
	//	glutSolidCube (1.0);
	//	glutWireTeapot (1.0);
	//	glutSolidTeapot (1.0);

	float time = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;
	while (time >= maxTime) time -= maxTime;
	std::cout << time << std::endl;

	glm::vec3 currentSplinePos = spline.GetCurrentPosition(time / maxTime);
	glm::vec3 currentSplineTangent = glm::normalize(spline.GetCurrentTangent(time / maxTime));
	glm::vec3 currentSplineSecondDeriv = glm::normalize(spline.GetCurrentSecondDerivative(time / maxTime));

	glm::vec3 normal = glm::normalize(glm::cross(currentSplineSecondDeriv, currentSplineTangent));
	glm::vec3 biNormal = glm::normalize(glm::cross(currentSplineTangent, normal));

	//depends on object
	glm::mat3x3 preRotationX = glm::mat3x3();
	preRotationX[0][0] = 1;
	preRotationX[0][1] = 0;
	preRotationX[0][2] = 0;
	preRotationX[1][0] = 0;
	preRotationX[1][1] = -1;
	preRotationX[1][2] = 0;
	preRotationX[2][0] = 0;
	preRotationX[2][1] = 0;
	preRotationX[2][2] = -1;

	glm::mat3x3 preRotationY = glm::mat3x3();
	preRotationY[0][0] = 0;
	preRotationY[0][1] = 0;
	preRotationY[0][2] = 1;
	preRotationY[1][0] = 0;
	preRotationY[1][1] = 1;
	preRotationY[1][2] = 0;
	preRotationY[2][0] = -1;
	preRotationY[2][1] = 0;
	preRotationY[2][2] = 0;

	glm::mat3x3 preRotation;//preRotationX * preRotationY;

	glm::mat3x3 rotMat = glm::mat3x3();
	rotMat[0][0] = currentSplineTangent.x;
	rotMat[0][1] = currentSplineTangent.y;
	rotMat[0][2] = currentSplineTangent.z;
	rotMat[1][0] = normal.x;
	rotMat[1][1] = normal.y;
	rotMat[1][2] = normal.z;
	rotMat[2][0] = biNormal.x;
	rotMat[2][1] = biNormal.y;
	rotMat[2][2] = biNormal.z;

	glm::mat3x3 finalRot = rotMat;// * preRotation;

	rotMat = glm::inverse(rotMat);
	glBegin(GL_LINE_STRIP);
	for (int i = 0; i < 10000; i++) {
		glm::vec3 current = spline.GetCurrentPosition(i / 10000.0f);
		glColor3ub((GLubyte)(0.0f), (GLubyte)(45.0f), (GLubyte)(85.0f));
		glVertex3f(current.x, current.y, current.z);
	}
	glEnd();

	glBegin(GL_LINES);
	glColor3ub((GLubyte)255.0f, (GLubyte)165.0f, (GLubyte)0.0f);
	glVertex3f(currentSplinePos.x, currentSplinePos.y, currentSplinePos.z);

	glColor3ub((GLubyte)255.0f, (GLubyte)165.0f, (GLubyte)0.0f);
	glVertex3f(currentSplinePos.x + currentSplineTangent.x, currentSplinePos.y + currentSplineTangent.y, currentSplinePos.z + currentSplineTangent.z);
	glEnd();

	glBegin(GL_TRIANGLES); // ili glBegin (GL_LINE_LOOP); za zicnu formu
	for (int i = 0; i < count; i += 9) {
		glm::vec3 vertex1;
		glm::vec3 vertex2;
		glm::vec3 vertex3;

		glm::vec3 normal1;
		glm::vec3 normal2;
		glm::vec3 normal3;

		for (unsigned int j = 0; j < 9; j += 3) {
			float x = vertices[i + j + 0];
			float y = vertices[i + j + 1];
			float z = vertices[i + j + 2];

			float nx = normals[i + j + 0];
			float ny = normals[i + j + 1];
			float nz = normals[i + j + 2];

			if (j == 0) {
				vertex1 = glm::vec4(x, y, z, 1.0f);
				normal1 = glm::vec3(nx, ny, nz);
			} else if (j == 3) {
				vertex2 = glm::vec4(x, y, z, 1.0f);
				normal2 = glm::vec3(nx, ny, nz);
			} else if (j == 6) {
				vertex3 = glm::vec4(x, y, z, 1.0f);
				normal3 = glm::vec3(nx, ny, nz);
			}
		}

		vertex1 = finalRot * vertex1;
		vertex2 = finalRot * vertex2;
		vertex3 = finalRot * vertex3;

		vertex1 += currentSplinePos;
		vertex2 += currentSplinePos;
		vertex3 += currentSplinePos;

		float lightAmount1;
		float lightAmount2;
		float lightAmount3;

		lightAmount1 = (glm::dot(normal1, glm::normalize(light - vertex1)) + 1) / 2.0f;
		lightAmount2 = (glm::dot(normal2, glm::normalize(light - vertex2)) + 1) / 2.0f;
		lightAmount3 = (glm::dot(normal3, glm::normalize(light - vertex3)) + 1) / 2.0f;

		lightAmount1 = lightAmount1 > 0.2f ? lightAmount1 : 0.2f;
		lightAmount2 = lightAmount2 > 0.2f ? lightAmount2 : 0.2f;
		lightAmount3 = lightAmount3 > 0.2f ? lightAmount3 : 0.2f;

		glm::vec3 lightToVert1 = vertex1 - light;
		glm::vec3 lightToVert2 = vertex2 - light;
		glm::vec3 lightToVert3 = vertex3 - light;

		glm::vec3 eyeToVert1 = vertex1 - ociste;
		glm::vec3 eyeToVert2 = vertex2 - ociste;
		glm::vec3 eyeToVert3 = vertex3 - ociste;

		glm::vec3 reflected1 = glm::normalize(lightToVert1 - 2 * glm::dot(lightToVert1, normal1) * normal1);
		glm::vec3 reflected2 = glm::normalize(lightToVert1 - 2 * glm::dot(lightToVert2, normal2) * normal2);
		glm::vec3 reflected3 = glm::normalize(lightToVert1 - 2 * glm::dot(lightToVert3, normal3) * normal3);

		float specular1 = glm::dot(reflected1, -eyeToVert1);
		float specular2 = glm::dot(reflected2, -eyeToVert2);
		float specular3 = glm::dot(reflected3, -eyeToVert3);

		specular1 = specular1 > 0.0f ? specular1 : 0.0f;
		specular2 = specular2 > 0.0f ? specular2 : 0.0f;
		specular3 = specular3 > 0.0f ? specular3 : 0.0f;

		glm::vec3 finalColor1 = color * lightAmount1 + glm::vec3(specular1);
		glm::vec3 finalColor2 = color * lightAmount2 + glm::vec3(specular2);
		glm::vec3 finalColor3 = color * lightAmount3 + glm::vec3(specular3);

		glColor3ub((GLubyte)finalColor1.x, (GLubyte)finalColor1.y, (GLubyte)finalColor1.z);
		glVertex3f(vertex1.x, vertex1.y, vertex1.z);

		glColor3ub((GLubyte)finalColor2.x, (GLubyte)finalColor2.y, (GLubyte)finalColor2.z);
		glVertex3f(vertex2.x, vertex2.y, vertex2.z);

		glColor3ub((GLubyte)finalColor3.x, (GLubyte)finalColor3.y, (GLubyte)finalColor3.z);
		glVertex3f(vertex3.x, vertex3.y, vertex3.z);
	}
	glEnd();
}

//*********************************************************************************
//	Mis.
//*********************************************************************************
void myMouse(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		glBegin(GL_TRIANGLES);
		for (int i = 0; i < count; i += 3) {
			float x = vertices[i];
			float y = vertices[i + 1];
			float z = vertices[i + 2];

			glVertex3f(x * width + width / 2.0f, y * height + height / 2.0f, 0.0f);
		}
		glEnd();

		glFlush();
	}

	//	Desna tipka - brise canvas. 
	else if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
		ociste.x = 0;
		updatePerspective();
		glutPostRedisplay();
	}
}

//*********************************************************************************
//	Tastatura tipke - r, g, b, k - mijenjaju boju.
//*********************************************************************************
void myKeyboard(unsigned char theKey, int mouseX, int mouseY) {
	switch (theKey) {
	case 'l':
		ociste.x = ociste.x + 0.1f;
		break;
	case 'k':
		ociste.x = ociste.x - 0.1f;
		break;
	case 'r':
		ociste.x = 0.0f;
		break;
	case 'i':
		if (idle) glutIdleFunc(nullptr);
		else glutIdleFunc(myIdle);
		idle = !idle;
		break;
	case 27:  //escape
		exit(0);
		break;
	}

	updatePerspective();
	glutPostRedisplay();
}


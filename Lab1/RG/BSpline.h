#pragma once
#include <GLM\mat4x4.hpp>
#include <GLM\vec4.hpp>
#include <vector>

class BSpline {

private:
	std::vector<glm::vec3> m_points;

public:
	BSpline() {
		m_points = std::vector<glm::vec3>();
	}
	BSpline(const std::string& t_filepath) {
		m_points = std::vector<glm::vec3>();

		std::string filePath = "..\\Res\\Curves\\" + t_filepath + ".txt";
		std::ifstream infile(filePath);

		std::string line;
		while (std::getline(infile, line)) {
			std::vector<std::string> vert = SplitString(line, ' ');
			m_points.push_back(glm::vec3(stof(vert[0]), stof(vert[1]) / 3.0f, stof(vert[2])));
		}
	}

	void AddPoint(float x, float y, float z) {
		m_points.push_back(glm::vec3(x, y, z));
	}

	/*t_time is between 0 and 1, the function automatically calculates the segment*/
	glm::vec3 GetCurrentPosition(float t_time) {
		glm::vec3 res = glm::vec3();
		int numOfPoints = m_points.size();
		if (numOfPoints < 4) return res;

		int numOfSegments = numOfPoints - 3;
		float timeForSegment = 1.0f / numOfSegments;
		int currentSegment = int(t_time / timeForSegment);
		float timeForCurrentSegment = (t_time - currentSegment * timeForSegment) / timeForSegment;

		glm::vec4 timeVec = glm::vec4(pow(timeForCurrentSegment, 3), pow(timeForCurrentSegment, 2), timeForCurrentSegment, 1);
		glm::mat4x4 matrix = glm::mat4x4();
		matrix[0][0] = -1;
		matrix[0][1] = 3;
		matrix[0][2] = -3;
		matrix[0][3] = 1;
		matrix[1][0] = 3;
		matrix[1][1] = -6;
		matrix[1][2] = 0;
		matrix[1][3] = 4;
		matrix[2][0] = -3;
		matrix[2][1] = 3;
		matrix[2][2] = 3;
		matrix[2][3] = 1;
		matrix[3][0] = 1;
		matrix[3][1] = 0;
		matrix[3][2] = 0;
		matrix[3][3] = 0;

		glm::mat3x4 pointMatrix = glm::mat3x4();
		pointMatrix[0][0] = m_points[currentSegment + 0].x;
		pointMatrix[0][1] = m_points[currentSegment + 1].x;
		pointMatrix[0][2] = m_points[currentSegment + 2].x;
		pointMatrix[0][3] = m_points[currentSegment + 3].x;
		pointMatrix[1][0] = m_points[currentSegment + 0].y;
		pointMatrix[1][1] = m_points[currentSegment + 1].y;
		pointMatrix[1][2] = m_points[currentSegment + 2].y;
		pointMatrix[1][3] = m_points[currentSegment + 3].y;
		pointMatrix[2][0] = m_points[currentSegment + 0].z;
		pointMatrix[2][1] = m_points[currentSegment + 1].z;
		pointMatrix[2][2] = m_points[currentSegment + 2].z;
		pointMatrix[2][3] = m_points[currentSegment + 3].z;

		res = timeVec * matrix * pointMatrix;
		return res / 6.0f;
	}
	glm::vec3 GetCurrentTangent(float t_time) {
		glm::vec3 res = glm::vec3();
		int numOfPoints = m_points.size();
		if (numOfPoints < 4) return res;

		int numOfSegments = numOfPoints - 3;
		float timeForSegment = 1.0f / numOfSegments;
		int currentSegment = int(t_time / timeForSegment);
		float timeForCurrentSegment = (t_time - currentSegment * timeForSegment) / timeForSegment;

		glm::vec3 timeVec = glm::vec3(pow(timeForCurrentSegment, 2), timeForCurrentSegment, 1);
		glm::mat4x3 matrix = glm::mat4x3();
		matrix[0][0] = -1;
		matrix[0][1] = 2;
		matrix[0][2] = -1;
		matrix[1][0] = 3;
		matrix[1][1] = -4;
		matrix[1][2] = 0;
		matrix[2][0] = -3;
		matrix[2][1] = 2;
		matrix[2][2] = 1;
		matrix[3][0] = 1;
		matrix[3][1] = 0;
		matrix[3][2] = 0;

		glm::mat3x4 pointMatrix = glm::mat3x4();
		pointMatrix[0][0] = m_points[currentSegment + 0].x;
		pointMatrix[0][1] = m_points[currentSegment + 1].x;
		pointMatrix[0][2] = m_points[currentSegment + 2].x;
		pointMatrix[0][3] = m_points[currentSegment + 3].x;
		pointMatrix[1][0] = m_points[currentSegment + 0].y;
		pointMatrix[1][1] = m_points[currentSegment + 1].y;
		pointMatrix[1][2] = m_points[currentSegment + 2].y;
		pointMatrix[1][3] = m_points[currentSegment + 3].y;
		pointMatrix[2][0] = m_points[currentSegment + 0].z;
		pointMatrix[2][1] = m_points[currentSegment + 1].z;
		pointMatrix[2][2] = m_points[currentSegment + 2].z;
		pointMatrix[2][3] = m_points[currentSegment + 3].z;

		res = timeVec * matrix * pointMatrix;
		return res / 2.0f;
	}
	glm::vec3 GetCurrentSecondDerivative(float t_time) {
		glm::vec3 res = glm::vec3();
		int numOfPoints = m_points.size();
		if (numOfPoints < 4) return res;

		int numOfSegments = numOfPoints - 3;
		float timeForSegment = 1.0f / numOfSegments;
		int currentSegment = int(t_time / timeForSegment);
		float timeForCurrentSegment = (t_time - currentSegment * timeForSegment) / timeForSegment;

		glm::vec2 timeVec = glm::vec2(timeForCurrentSegment, 1);
		glm::mat4x2 matrix = glm::mat4x2();
		matrix[0][0] = -1;
		matrix[0][1] = 1;
		matrix[1][0] = 3;
		matrix[1][1] = -2;
		matrix[2][0] = -3;
		matrix[2][1] = 1;
		matrix[3][0] = 1;
		matrix[3][1] = 0;

		glm::mat3x4 pointMatrix = glm::mat3x4();
		pointMatrix[0][0] = m_points[currentSegment + 0].x;
		pointMatrix[0][1] = m_points[currentSegment + 1].x;
		pointMatrix[0][2] = m_points[currentSegment + 2].x;
		pointMatrix[0][3] = m_points[currentSegment + 3].x;
		pointMatrix[1][0] = m_points[currentSegment + 0].y;
		pointMatrix[1][1] = m_points[currentSegment + 1].y;
		pointMatrix[1][2] = m_points[currentSegment + 2].y;
		pointMatrix[1][3] = m_points[currentSegment + 3].y;
		pointMatrix[2][0] = m_points[currentSegment + 0].z;
		pointMatrix[2][1] = m_points[currentSegment + 1].z;
		pointMatrix[2][2] = m_points[currentSegment + 2].z;
		pointMatrix[2][3] = m_points[currentSegment + 3].z;

		res = timeVec * matrix * pointMatrix;
		return res;
	}
};
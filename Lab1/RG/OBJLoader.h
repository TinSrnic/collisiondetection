#pragma once
#include <string>
#include <vector>
#include <fstream>
#include <map>

#include <GLUT\freeglut.h>
#include <GLM\vec3.hpp>

#include "OBJLoader.h"
#include "StringManipulation.h"

void LoadOBJ(std::string filepath, GLfloat*& verticesOutput, GLfloat*& normalsOutput, int& count) {
	std::string filePath = "..\\Res\\Models\\" + filepath + ".obj";
	std::ifstream infile(filePath);

	std::vector<glm::vec3> vertices = std::vector<glm::vec3>();
	std::vector<glm::ivec3> faces = std::vector<glm::ivec3>();
	std::map<int, std::vector<glm::vec3>> vertexToPolyNormal = std::map<int, std::vector<glm::vec3>>();

	GLfloat *verticesArray = nullptr;
	GLfloat *vertNormalsArray = nullptr;

	float minX = 100000.0f;
	float maxX = -100000.0f;

	float minY = 100000.0f;
	float maxY = -100000.0f;

	float minZ = 100000.0f;
	float maxZ = -100000.0f;

	std::string line;
	unsigned int currentIndCount = 0;
	while (std::getline(infile, line)) {
		std::vector<std::string> currentLine = SplitString(line, ' ');
		if (currentLine.at(0) == "v") {
			float x = std::stof(currentLine[1]);
			float y = std::stof(currentLine[2]);
			float z = std::stof(currentLine[3]);

			vertices.push_back(glm::vec3(x, y, z));
			if (minX > x) minX = x;
			else if (maxX < x) maxX = x;
			if (minY > y) minY = y;
			else if (maxY < y) maxY = y;
			if (minZ > z) minZ = z;
			else if (maxZ < z) maxZ = z;
		} else if (currentLine.at(0) == "f") {
			faces.push_back(glm::vec3(std::stoi(currentLine[1]) - 1, std::stoi(currentLine[2]) - 1, std::stoi(currentLine[3]) - 1));
		}
	}

	verticesArray = (GLfloat*)malloc(sizeof(GLfloat) * faces.size() * 3 * 3);
	vertNormalsArray = (GLfloat*)malloc(sizeof(GLfloat) * faces.size() * 3 * 3);

	float width = maxX - minX;
	float height = maxY - minY;
	float length = maxZ - minZ;

	float maxDimension = width;
	if (maxDimension < height) maxDimension = height;
	if (maxDimension < length) maxDimension = length;

	int pointer = 0;
	for (glm::ivec3 face : faces) {
		glm::vec3 normalized1 = glm::vec3(vertices.at(face.x).x, vertices.at(face.x).y, vertices.at(face.x).z);
		glm::vec3 normalized2 = glm::vec3(vertices.at(face.y).x, vertices.at(face.y).y, vertices.at(face.y).z);
		glm::vec3 normalized3 = glm::vec3(vertices.at(face.z).x, vertices.at(face.z).y, vertices.at(face.z).z);

		verticesArray[pointer * 9 + 0] = normalized1.x / maxDimension;
		verticesArray[pointer * 9 + 1] = normalized1.y / maxDimension;
		verticesArray[pointer * 9 + 2] = normalized1.z / maxDimension;

		verticesArray[pointer * 9 + 3] = normalized2.x / maxDimension;
		verticesArray[pointer * 9 + 4] = normalized2.y / maxDimension;
		verticesArray[pointer * 9 + 5] = normalized2.z / maxDimension;

		verticesArray[pointer * 9 + 6] = normalized3.x / maxDimension;
		verticesArray[pointer * 9 + 7] = normalized3.y / maxDimension;
		verticesArray[pointer * 9 + 8] = normalized3.z / maxDimension;

		glm::vec3 v1 = normalized2 - normalized1;
		glm::vec3 v2 = normalized3 - normalized1;
		glm::vec3 normal = glm::normalize(glm::cross(v1, v2));

		if (vertexToPolyNormal.find(face.x) != vertexToPolyNormal.end()) {
			vertexToPolyNormal.at(face.x).push_back(normal);
		} else {
			std::vector<glm::vec3> newVector = std::vector<glm::vec3>();
			newVector.push_back(normal);
			vertexToPolyNormal.insert(std::pair<int, std::vector<glm::vec3>>(face.x, newVector));
		}

		if (vertexToPolyNormal.find(face.y) != vertexToPolyNormal.end()) {
			vertexToPolyNormal.at(face.y).push_back(normal);
		} else {
			std::vector<glm::vec3> newVector = std::vector<glm::vec3>();
			newVector.push_back(normal);
			vertexToPolyNormal.insert(std::pair<int, std::vector<glm::vec3>>(face.y, newVector));
		}

		if (vertexToPolyNormal.find(face.z) != vertexToPolyNormal.end()) {
			vertexToPolyNormal.at(face.y).push_back(normal);
		} else {
			std::vector<glm::vec3> newVector = std::vector<glm::vec3>();
			newVector.push_back(normal);
			vertexToPolyNormal.insert(std::pair<int, std::vector<glm::vec3>>(face.z, newVector));
		}

		++pointer;
	}

	pointer = 0;
	for (glm::ivec3 face : faces) {
		std::vector<glm::vec3> normalsList1 = vertexToPolyNormal.at(face.x);
		std::vector<glm::vec3> normalsList2 = vertexToPolyNormal.at(face.y);
		std::vector<glm::vec3> normalsList3 = vertexToPolyNormal.at(face.z);

		glm::vec3 normal1 = glm::vec3(0.0f);
		glm::vec3 normal2 = glm::vec3(0.0f);
		glm::vec3 normal3 = glm::vec3(0.0f);

		for (glm::vec3 normal : normalsList1) normal1 += normal;
		normal1 /= normalsList1.size();

		for (glm::vec3 normal : normalsList2) normal2 += normal;
		normal2 /= normalsList2.size();

		for (glm::vec3 normal : normalsList3) normal3 += normal;
		normal3 /= normalsList3.size();

		vertNormalsArray[pointer * 9 + 0] = normal1.x;
		vertNormalsArray[pointer * 9 + 1] = normal1.y;
		vertNormalsArray[pointer * 9 + 2] = normal1.z;

		vertNormalsArray[pointer * 9 + 3] = normal2.x;
		vertNormalsArray[pointer * 9 + 4] = normal2.y;
		vertNormalsArray[pointer * 9 + 5] = normal2.z;

		vertNormalsArray[pointer * 9 + 6] = normal3.x;
		vertNormalsArray[pointer * 9 + 7] = normal3.y;
		vertNormalsArray[pointer * 9 + 8] = normal3.z;

		++pointer;
	}

	count = (int)faces.size() * 3 * 3;
	verticesOutput = verticesArray;
	normalsOutput = vertNormalsArray;
}
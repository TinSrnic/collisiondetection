#include "Window.h"
#include <stdio.h>
#include <iostream>
#include <string>
#include "ParticleRenderer.h"
#include <GLM\mat3x3.hpp>
#include <GLM\vec3.hpp>
#include <GLM\gtc\matrix_transform.hpp>
#define _USE_MATH_DEFINES
#include <math.h>
#include "OBJLoader.h"
#include "BSpline.h"

bool shouldClose = false;

int width = 800;
int height = 600;

float prevTime = 0.0f;

Window window = Window(width, height);
ParticleShader shader = ParticleShader("..\\Res\\Shaders\\particleVert.txt", "..\\Res\\Shaders\\particleFrag.txt");
ParticleRenderer renderer = ParticleRenderer(shader);
ParticleEmitter emitter1 = ParticleEmitter(glm::vec3(-4.0f, -1.0f, -4.0f), "..\\Res\\Textures\\particleStar.png", 1);
ParticleEmitter emitter2 = ParticleEmitter(glm::vec3(0.0f, -1.0f, -4.0f), "..\\Res\\Textures\\fire.png", 8);
ParticleEmitter emitter3 = ParticleEmitter(glm::vec3(4.0f, -1.0f, -4.0f), "..\\Res\\Textures\\cosmic.png", 4);

glm::mat4x4 projectionMatrix = glm::mat4x4(0.0f);
glm::mat4x4 viewMatrix = glm::mat4x4(1.0f);

glm::vec3 ociste = glm::vec3(0.0f, 0.0f, 0.0f);

glm::vec3 color = glm::vec3(135, 206, 250);
glm::vec3 light = glm::vec3(-1.0f, 0.0f, -1.0f);

glm::mat4x4 MakePerspective(float FOV, float aspect, float nearPlane, float farPlane) {
	glm::mat4x4 perspective = glm::mat4x4(0.0f);
	float FOVrad = (float)(FOV * M_PI / 180.0f);

	perspective[0][0] = 1 / (aspect * tan(FOVrad / 2));
	perspective[0][1] = 0;
	perspective[0][2] = 0;
	perspective[0][3] = 0;
	perspective[1][0] = 0;
	perspective[1][1] = 1 / (tan(FOVrad / 2));
	perspective[1][2] = 0;
	perspective[1][3] = 0;
	perspective[2][0] = 0;
	perspective[2][1] = 0;
	perspective[2][2] = -1 * (farPlane + nearPlane) / (farPlane - nearPlane);
	perspective[2][3] = -1 * (2 * farPlane * nearPlane) / (farPlane - nearPlane);
	perspective[3][0] = 0;
	perspective[3][1] = 0;
	perspective[3][2] = -1;
	perspective[3][3] = 0;

	return perspective;
}

//*********************************************************************************
//	Function Prototypes.
//*********************************************************************************
void myDisplay();
void myReshape(int width, int height);
void myKeyboard(unsigned char theKey, int mouseX, int mouseY);

//*********************************************************************************
//	Glavni program.
//*********************************************************************************
int main(int argc, char** argv) {
	srand((unsigned int)time(NULL));
	ociste = glm::vec3(0.0f, 0.0f, 0.0f);

	glutReshapeFunc(myReshape);
	glutDisplayFunc(myDisplay);
	glutKeyboardFunc(myKeyboard);

	projectionMatrix = MakePerspective(70.0f, 16.0f / 9.0f, 0.1f, 1000.0f);
	renderer.SetProjection(projectionMatrix);
	renderer.AddEmitter(emitter1);
	renderer.AddEmitter(emitter2);
	renderer.AddEmitter(emitter3);

	while (!shouldClose) {
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);		         // boja pozadine - bijela
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		viewMatrix[0][3] = ociste.x;
		viewMatrix[1][3] = ociste.y;
		viewMatrix[2][3] = ociste.z;

		//viewMatrix[0][0] = 0.5f;
		//viewMatrix[0][2] = -0.866f;
		//viewMatrix[2][0] = 0.866f;
		//viewMatrix[2][2] = 0.5f;

		//ociste.x += 0.01f;
		//ociste.z += 0.01f;

		renderer.Update(glutGet(GLUT_ELAPSED_TIME) / 1000.0f - prevTime);
		prevTime = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;
		renderer.Render(viewMatrix);

		glutSwapBuffers();      // iscrtavanje iz dvostrukog spemnika (umjesto glFlush)
	}

	return 0;
}

//*********************************************************************************
//	Osvjezavanje prikaza. (nakon preklapanja prozora) 
//*********************************************************************************
void myDisplay() {
	//printf("Pozvan myDisplay()\n");
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);		         // boja pozadine - bijela
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glutSwapBuffers();      // iscrtavanje iz dvostrukog spemnika (umjesto glFlush)
}

//*********************************************************************************
//	Promjena velicine prozora.
//*********************************************************************************
void myReshape(int w, int h) {
	width = w; height = h;
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);        // aktivirana matrica projekcije
	glLoadIdentity();
	gluPerspective(45.0, (float)width / height, 0.5, 8.0); // kut pogleda, x/y, prednja i straznja ravnina odsjecanja
	gluLookAt(ociste.x, ociste.y, ociste.z, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);	// ociste x,y,z; glediste x,y,z; up vektor x,y,z
	glMatrixMode(GL_MODELVIEW);         // aktivirana matrica modela
}

//*********************************************************************************
//	Tastatura tipke - r, g, b, k - mijenjaju boju.
//*********************************************************************************
void myKeyboard(unsigned char theKey, int mouseX, int mouseY) {
	switch (theKey) {
	case 'w':
		ociste.x = ociste.z - 0.1f;
		break;
	case 'a':
		ociste.x = ociste.x - 0.1f;
		break;
	case 's':
		ociste.x = ociste.z + 0.1f;
		break;
	case 'd':
		ociste.x = ociste.x + 0.1f;
		break;
	case 'r':
		ociste.x = 0.0f;
		break;
	case 27:  //escape
		shouldClose = true;
		break;
	}

	glutPostRedisplay();
}
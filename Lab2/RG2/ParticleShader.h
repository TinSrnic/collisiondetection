#pragma once
#include "ShaderProgram.h"

/*Class for link with particle shader*/
class ParticleShader : public ShaderProgram {

private:
	GLint m_projectionMatrix;
	GLint m_viewMatrix;
	GLint m_inverseViewMatrix;
	GLint m_numberOfRows;

public:
	ParticleShader(const std::string& t_vertPath, const std::string& t_fragPath) : ShaderProgram(t_vertPath, t_fragPath) {
		m_projectionMatrix = glGetUniformLocation(m_programID, "projectionMatrix");
		m_viewMatrix = glGetUniformLocation(m_programID, "viewMatrix");
		m_inverseViewMatrix = glGetUniformLocation(m_programID, "inverseViewMatrix");
		m_numberOfRows = glGetUniformLocation(m_programID, "numberOfRows");
	}

	void LoadProjectionMatrix(const glm::mat4x4& t_mat) const { SetUniformMat4(m_projectionMatrix, t_mat); }
	void LoadViewMatrix(const glm::mat4x4& t_mat) const { SetUniformMat4(m_viewMatrix, t_mat); }
	void LoadInverseViewMatrix(const glm::mat4x4& t_mat) const { SetUniformMat4(m_inverseViewMatrix, t_mat); }
	void LoadAtlasRows(unsigned int t_numberOfRows) const { SetUniform1f(m_numberOfRows, (float)t_numberOfRows); }
};
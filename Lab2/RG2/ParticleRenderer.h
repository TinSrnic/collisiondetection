#pragma once
#include <vector>
#include "ParticleEmitter.h"

class ParticleRenderer {

private:
	ParticleShader & m_particleShader;
	std::vector<ParticleEmitter> m_emitters;

public:
	ParticleRenderer(ParticleShader& t_particleShader) : m_particleShader(t_particleShader) {
		m_emitters = std::vector<ParticleEmitter>();
	}

	void AddEmitter(ParticleEmitter emitter) {
		m_emitters.push_back(emitter);
	}

	void Update(float t_elapsedTime) {
		for (std::vector<ParticleEmitter>::iterator it = m_emitters.begin(); it != m_emitters.end(); ++it) {
			it->Update(t_elapsedTime);
		}
	}

	void Render(glm::mat4x4 t_viewMatrix) {
		m_particleShader.BindShader();

		glEnable(GL_BLEND);
		glDepthMask(false); //don't render to the depth buffer
		glm::mat4x4 onlyRot = glm::mat4x4(t_viewMatrix);
		onlyRot[0][3] = 0.0;
		onlyRot[1][3] = 0.0;
		onlyRot[2][3] = 0.0;

		m_particleShader.LoadViewMatrix(t_viewMatrix);
		m_particleShader.LoadInverseViewMatrix(glm::inverse(onlyRot));

		for (ParticleEmitter emitter : m_emitters) {
			Prepare(emitter.GetVAO());
			PrepareTexture(emitter.GetTexture(), emitter.GetNumRows());
			emitter.FillData();
			emitter.Render();
		}

		Unbind();
		m_particleShader.UnbindShader();
	}

	void SetProjection(glm::mat4x4 t_projectionMatrix) {
		m_particleShader.BindShader();
		m_particleShader.LoadProjectionMatrix(t_projectionMatrix);
		m_particleShader.UnbindShader();
	}

private:
	void Prepare(GLuint t_vao) {
		glBindVertexArray(t_vao);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);
		glEnableVertexAttribArray(4);
		glEnableVertexAttribArray(5);
		glEnableVertexAttribArray(6);
	}
	void PrepareTexture(GLuint t_texture, unsigned int t_numRows) {
		m_particleShader.LoadAtlasRows(t_numRows);

		//TODO: look into what these functions do
		if (0) {
			glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		} else {
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);  //use addative blending so that the order in which particles are rendered doesn't matter
		}

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, t_texture);
	}
	void Unbind() {
		glDepthMask(true);
		glDisable(GL_BLEND);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(3);
		glDisableVertexAttribArray(4);
		glDisableVertexAttribArray(5);
		glDisableVertexAttribArray(6);
		glBindVertexArray(0);
	}
};
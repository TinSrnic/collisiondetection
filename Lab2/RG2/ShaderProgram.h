#pragma once
#include <GLEW\glew.h>
#include <GLUT\freeglut.h>
#include <GLM\vec4.hpp>
#include <GLM\mat4x4.hpp>
#include <vector>
#include "FileReader.h"

class ShaderProgram {

protected:
	GLuint m_vertID;
	GLuint m_fragID;
	GLuint m_programID;

protected:
	ShaderProgram(const std::string& vertPath, const std::string& fragPath) {
		m_vertID = glCreateShader(GL_VERTEX_SHADER);
		m_fragID = glCreateShader(GL_FRAGMENT_SHADER);

		// Read the Vertex Shader code from the file
		std::string VertexShaderCode = ReadFile(vertPath);
		// Read the Fragment Shader code from the file
		std::string FragmentShaderCode = ReadFile(fragPath);

		GLint Result = GL_FALSE;
		int InfoLogLength;

		// Compile Vertex Shader
		char const *VertexSourcePointer = VertexShaderCode.c_str();
		glShaderSource(m_vertID, 1, &VertexSourcePointer, NULL);
		glCompileShader(m_vertID);

		// Check Vertex Shader
		glGetShaderiv(m_vertID, GL_COMPILE_STATUS, &Result);
		glGetShaderiv(m_vertID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		if (InfoLogLength > 0) {
			std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
			glGetShaderInfoLog(m_vertID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
			std::cerr << &VertexShaderErrorMessage[0] << std::endl;
		}

		// Compile Fragment Shader
		char const * FragmentSourcePointer = FragmentShaderCode.c_str();
		glShaderSource(m_fragID, 1, &FragmentSourcePointer, NULL);
		glCompileShader(m_fragID);

		// Check Fragment Shader
		glGetShaderiv(m_fragID, GL_COMPILE_STATUS, &Result);
		glGetShaderiv(m_fragID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		if (InfoLogLength > 0) {
			std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
			glGetShaderInfoLog(m_fragID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
			std::cerr << &FragmentShaderErrorMessage[0];
		}

		// Link the program
		m_programID = glCreateProgram();
		glAttachShader(m_programID, m_vertID);
		glAttachShader(m_programID, m_fragID);
		glLinkProgram(m_programID);

		// Check the program
		glGetProgramiv(m_programID, GL_LINK_STATUS, &Result);
		glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		if (InfoLogLength > 0) {
			std::vector<char> ProgramErrorMessage(InfoLogLength + 1);
			glGetProgramInfoLog(m_programID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
			std::cerr << &ProgramErrorMessage[0];
		}
	}

	void SetUniform1i(GLint location, int value) const { glUniform1i(location, value); }
	void SetUniform1f(GLint location, float value) const { glUniform1f(location, value); }
	void SetUniformVec2(GLint location, const glm::vec2& vec) const { glUniform2f(location, vec.x, vec.y); }
	void SetUniformVec3(GLint location, const glm::vec3& vec) const { glUniform3f(location, vec.x, vec.y, vec.z); }
	void SetUniformVec4(GLint location, const glm::vec4& vec) const { glUniform4f(location, vec.x, vec.y, vec.z, vec.w); }
	void SetUniformMat4(GLint location, const glm::mat4x4& mat) const { 
		float matrix[16];

		matrix[0] = mat[0].x;
		matrix[1] = mat[1].x;
		matrix[2] = mat[2].x;
		matrix[3] = mat[3].x;
		matrix[4] = mat[0].y;
		matrix[5] = mat[1].y;
		matrix[6] = mat[2].y;
		matrix[7] = mat[3].y;
		matrix[8] = mat[0].z;
		matrix[9] = mat[1].z;
		matrix[10] = mat[2].z;
		matrix[11] = mat[3].z;
		matrix[12] = mat[0].w;
		matrix[13] = mat[1].w;
		matrix[14] = mat[2].w;
		matrix[15] = mat[3].w;
		
		glUniformMatrix4fv(location, 1, GL_FALSE, matrix); 
	}

public:
	void BindShader() { glUseProgram(m_programID); }
	void UnbindShader() { glUseProgram(0); }

	void CleanUp() {
		UnbindShader();
		glDetachShader(m_programID, m_vertID);
		glDetachShader(m_programID, m_fragID);

		glDeleteShader(m_vertID);
		glDeleteShader(m_fragID);

		glDeleteProgram(m_programID);
	}
};
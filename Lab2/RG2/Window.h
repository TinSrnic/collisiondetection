#pragma once
#include <GLEW\glew.h>
#include <GLUT\freeglut.h>
#include <iostream>

class Window {

private:
	GLuint m_window;

public:
	Window(int width, int height) {
		glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
		glutInitWindowSize(width, height);
		glutInitWindowPosition(100, 100);
		int argc = 1;
		char *argv[1] = { (char*)"Something" };
		glutInit(&argc, argv);

		m_window = glutCreateWindow("Tijelo");
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);

		if (glewInit() != GLEW_OK) {
			std::cout << "Failed to initilize GLEW!";
		}
	}
};
#pragma once
#include <GLM\mat3x3.hpp>
#include <GLM\vec3.hpp>
#include <GLM\gtc\matrix_transform.hpp>
#define GRAVITY -10

class Particle {

private:
	glm::vec3 m_position;
	glm::vec3 m_scale;
	glm::vec3 m_velocity;
	float m_gravityEffect; //1 is full gravity
	float m_currentLife;
	float m_lifespan;

	float m_distanceFromCamera;

public:
	Particle(glm::vec3 t_position, glm::vec3 t_scale, glm::vec3 t_velocity, float t_gravityEffect, float t_lifespan, glm::vec3 t_camPos) {
		m_position = t_position;
		m_scale = t_scale;
		m_velocity = t_velocity;
		m_gravityEffect = t_gravityEffect;
		m_currentLife = 0.0f;
		m_lifespan = t_lifespan;

		m_distanceFromCamera = (m_position - t_camPos).length();
	}

	glm::vec3 GetPosition() const { return m_position; }
	glm::vec3 GetScale() const { return m_scale; }

	bool Update(float t_elapsedTime, glm::vec3 t_emitterPos, std::vector<glm::vec4> t_forcePoints, glm::vec3 t_camPos) {
		m_currentLife += t_elapsedTime;
		if (m_currentLife > m_lifespan) return false;

		glm::vec3 accel = glm::vec3(0.0f);
		glm::vec3 force = glm::vec3(0.0f);
		for (glm::vec4 forcePoint : t_forcePoints) {
			glm::vec3 forceVec = glm::vec3(forcePoint);
			force += ((forceVec + t_emitterPos) - m_position) * forcePoint.w;
		}

		accel = force / 1.0f; //100 is mass
		accel.y += m_gravityEffect * GRAVITY;
		accel = accel * t_elapsedTime;
		m_velocity += accel;
		m_position += m_velocity * t_elapsedTime;

		m_distanceFromCamera = (m_position - t_camPos).length();

		return true;
	}

	void GetOffsetAndBlend(glm::vec4& t_offset, float& t_blend, unsigned int t_numOfRows) const {
		//TODO: optimize
		if (t_numOfRows == 1) {
			t_offset = glm::vec4(0, 0, 0, 0);
			t_blend = 1.0;
			return;
		}

		unsigned int numberOfRows = t_numOfRows;
		float unit = m_lifespan / (numberOfRows * numberOfRows);
		unsigned int squareNumber = (unsigned int)(m_currentLife / unit);

		unsigned int row1 = squareNumber / numberOfRows;
		unsigned int column1 = squareNumber % numberOfRows;

		unsigned int nextSquare;
		nextSquare = (squareNumber + 1 > numberOfRows * numberOfRows) ? squareNumber : squareNumber + 1;

		unsigned int row2 = nextSquare / numberOfRows;
		unsigned int column2 = nextSquare % numberOfRows;

		t_offset = glm::vec4((float)column1 / numberOfRows, (float)(numberOfRows - row1 - 1) / numberOfRows, (float)column2 / numberOfRows, (float)(numberOfRows - row2 - 1) / numberOfRows);
		t_blend = (float)(((squareNumber + 1) * unit - m_currentLife) / unit);
	}

	static bool CompareParticles(const Particle& t_first, const Particle& t_second) {
		return t_first.m_distanceFromCamera > t_second.m_distanceFromCamera;
	}
};
#pragma once
#include "ParticleShader.h"
#include <SOIL\SOIL.h>
#include <vector>
#include <list>
#include <cstdlib>
#include <time.h>
#include "Particle.h"
#include "BSpline.h"

class ParticleEmitter {

private:
	std::list<Particle> m_particles;
	std::vector<GLfloat> m_vboData = std::vector<GLfloat>();

	GLuint m_texture;
	unsigned int m_numberOfRows;
	bool m_useAddative;

	glm::vec3 m_position;
	glm::vec3 m_scale;
	glm::vec3 m_velocity;
	float m_gravityEffect;
	float m_lifespan;

	float m_pps; //Particles per second
	float m_totalElapsedTime;

	GLuint m_vaoID; // all particles use the same rawModel, so it's stored here
	GLuint m_vboID; // vbo used for all instanced rendering

	BSpline m_spline;
	float m_totalTime;

	std::vector<glm::vec4> m_forcePoints;

	unsigned int m_maxParticles = 10000;
	unsigned int m_bytesPerParticle = 21; //21 bytes = 16 for model matrix, 4 for offsets, 1 for blend factor

public:
	ParticleEmitter(glm::vec3 t_position, const std::string& t_texturePath, const unsigned int t_numberOfRows) {
		m_particles = std::list<Particle>();
		
		m_position = t_position;
		m_scale = glm::vec3(1.0f, 1.0f, 1.0f);
		m_velocity = glm::vec3(1.0f, 0.5f, 0.0f);
		m_gravityEffect = 0.02f;
		m_totalElapsedTime = 0.0f;
		m_lifespan = 3.0f;
		m_pps = 100.0f;
		m_useAddative = false;

		m_forcePoints.push_back(glm::vec4(-1.0f, 1.0f, 0.0f, ((float)rand() / RAND_MAX)) * 1.5f);
		m_forcePoints.push_back(glm::vec4(1.0f, 1.0f, 0.0f, ((float)rand() / RAND_MAX)) * 1.5f);
		m_forcePoints.push_back(glm::vec4(0.0f, 1.0f, 1.0f, ((float)rand() / RAND_MAX)) * 1.5f);
		m_forcePoints.push_back(glm::vec4(0.0f, 1.0f, -1.0f, ((float)rand() / RAND_MAX)) * 1.5f);
		m_forcePoints.push_back(glm::vec4(0.0f, 2.0f, 0.0f, ((float)rand() / RAND_MAX)) * 1.5f);

		m_numberOfRows = t_numberOfRows;
		m_texture = SOIL_load_OGL_texture(
			t_texturePath.c_str(),
			SOIL_LOAD_AUTO,
			SOIL_CREATE_NEW_ID,
			SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
		);

		m_spline = BSpline("spiralTrue");

		const GLfloat vertices[8] = { -0.5f, 0.5f, -0.5f, -0.5f, 0.5f, 0.5f, 0.5f, -0.5f };
		GLuint vertBufferID;

		glGenVertexArrays(1, &m_vaoID);
		glBindVertexArray(m_vaoID);

		//position buffer
		glGenBuffers(1, &vertBufferID);

		glBindBuffer(GL_ARRAY_BUFFER, vertBufferID);
		glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(GLfloat), vertices, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

		//instanced buffer
		glGenBuffers(1, &m_vboID);
		glBindBuffer(GL_ARRAY_BUFFER, m_vboID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * m_maxParticles * m_bytesPerParticle, 0, GL_STREAM_DRAW); //stream draw means the data changes frequently, which it does

																												 //REMEMBER, stride is when the rendering is done how many bytes it has to go ahead and offset is where that element is located in the current part of the vbo
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * m_bytesPerParticle, (void*)(0 * 4 * sizeof(GLfloat)));
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * m_bytesPerParticle, (void*)(1 * 4 * sizeof(GLfloat)));
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * m_bytesPerParticle, (void*)(2 * 4 * sizeof(GLfloat)));
		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * m_bytesPerParticle, (void*)(3 * 4 * sizeof(GLfloat)));
		glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * m_bytesPerParticle, (void*)(4 * 4 * sizeof(GLfloat)));
		glVertexAttribPointer(6, 1, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * m_bytesPerParticle, (void*)(5 * 4 * sizeof(GLfloat)));

		glVertexAttribDivisor(1, 1); //the second number is how many instances it has to render before it increments
		glVertexAttribDivisor(2, 1);
		glVertexAttribDivisor(3, 1);
		glVertexAttribDivisor(4, 1);
		glVertexAttribDivisor(5, 1);
		glVertexAttribDivisor(6, 1);

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}

	std::list<Particle>& GetParticles() { return m_particles; }

	void Update(float t_elapsedTime) {
		m_totalTime += t_elapsedTime;
		while (m_totalTime >= 10.0f) m_totalTime -= 10.0f;
		glm::vec3 emitterPos = m_position + m_spline.GetCurrentPosition(m_totalTime / 10.0f);
		emitterPos.z -= 4.0f;

		std::list<Particle>::iterator it = m_particles.begin();
		while (it != m_particles.end()) {
			if (!it->Update(t_elapsedTime, emitterPos, m_forcePoints, glm::vec3(0.0f))) {
				it = m_particles.erase(it);
			} else {
				++it;
			}
		}

		//TODO: optimize this
		m_totalElapsedTime += t_elapsedTime;
		unsigned int particlesToCreate = 0;
		float unit = 1.0f / m_pps;

		while (m_totalElapsedTime >= unit) {
			m_totalElapsedTime -= unit;
			particlesToCreate++;
		}

		for (unsigned int x = 0; x < particlesToCreate; x++) {
			glm::vec3 randPos = glm::vec3((float)rand() / RAND_MAX, (float)rand() / RAND_MAX, (float)rand() / RAND_MAX);
			glm::vec3 randScale = glm::vec3((float)rand() / RAND_MAX, (float)rand() / RAND_MAX, (float)rand() / RAND_MAX);
			glm::vec3 randVel = glm::vec3((float)rand() / RAND_MAX, (float)rand() / RAND_MAX, (float)rand() / RAND_MAX);
			float randLife = (float)rand() / RAND_MAX;
			float randGrav = (float)rand() / RAND_MAX;

			randPos = (randPos - glm::vec3(0.5f)) * 0.2f;
			randScale = (randScale - glm::vec3(0.5f)) * 0.2f;
			randVel = (randVel - glm::vec3(0.5f)) * 0.2f;
			randLife = (randLife - 0.5f) * 0.2f;
			randGrav = (randGrav - 0.5f) * 0.2f;

			m_particles.push_back(Particle(emitterPos + randPos, m_scale + randScale, m_velocity + randVel, m_gravityEffect + randGrav, m_lifespan + randLife, glm::vec3(0.0f)));
		}

		m_particles.sort(Particle::CompareParticles);
	}
	void FillData() {
		m_vboData.clear(); //one cycle of vbo filling should fill it up with data of all the particles which use the same texture
		for (Particle particle : m_particles) {
			glm::mat4x4 modelMatrix = MakeModelMatrix(particle);
			for (unsigned int i = 0; i < 4; ++i) {
				for (unsigned int j = 0; j < 4; ++j) {
					m_vboData.push_back(modelMatrix[j][i]);
				}
			}

			glm::vec4 offset;
			float blend;
			particle.GetOffsetAndBlend(offset, blend, m_numberOfRows);

			m_vboData.push_back(offset.x);
			m_vboData.push_back(offset.y);
			m_vboData.push_back(offset.z);
			m_vboData.push_back(offset.w);
			m_vboData.push_back(blend);
		}
	}
	void Render() {
		if (m_vboData.size() != 0) {
			glBindBuffer(GL_ARRAY_BUFFER, m_vboID);
			glBufferSubData(GL_ARRAY_BUFFER, 0, m_vboData.size() * sizeof(GLfloat), &m_vboData[0]);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, (GLsizei)(m_vboData.size() / m_bytesPerParticle));
		}
	}
	
	unsigned int GetNumOfParticles() { return (unsigned int)m_particles.size(); }
	GLuint GetTexture() { return m_texture; }
	unsigned int GetNumRows() { return m_numberOfRows; }
	GLuint GetVAO() { return m_vaoID; }

private:
	glm::mat4x4 MakeModelMatrix(const Particle& particle) {
		glm::mat4x4 res = glm::mat4x4(0.0f);
		glm::vec3 position = particle.GetPosition();
		glm::vec3 scale = particle.GetScale();

		res[0][0] = scale.x;
		res[1][1] = scale.y;
		res[2][2] = scale.z;
		res[3][3] = 1.0f;

		res[0][3] = position.x;
		res[1][3] = position.y;
		res[2][3] = position.z;

		return res;
	}
};
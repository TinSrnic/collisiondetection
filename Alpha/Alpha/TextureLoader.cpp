#include "TextureLoader.h"

std::map<std::string, GLuint> TextureLoader::m_textures = std::map<std::string, GLuint>();
std::map<std::vector<std::string>, GLuint> TextureLoader::m_cubeTextures = std::map<std::vector<std::string>, GLuint>();
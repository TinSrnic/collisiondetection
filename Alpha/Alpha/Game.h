#pragma once
#include "Engine.h"
#include "PhysicsEngine.h"
#include "GameObjects.h"

//Class for storing all the object which will then be rendered
class Game {

public:
	Engine& m_engine;
	PhysicsEngine& m_physicsEngine;
	bool m_collision;

public:
	Game(Engine& engine, PhysicsEngine& physicsEngine) : m_engine(engine),  m_physicsEngine(physicsEngine) {
		/*m_audioMaster.AddBuffer("..\\Res\\Sound\\main.wav", "main");
		m_ambientMusic.ChangeSound(m_audioMaster.GetBuffer("main"));
		m_ambientMusic.ChangeVolume(0.1f);
		m_ambientMusic.Play();*/
		m_collision = false;
	}

	void PushToEngine() {
		if (m_collision) {
			for (int i = 0; i < GameObjects::m_boundedEntities.size(); ++i) GameObjects::m_boundedEntities[i]->Update();
			for(int i = 0; i < 10; ++i) m_physicsEngine.Update(GameObjects::m_boundedEntities);
		}
		m_engine.Render(GameObjects::m_terrains, GameObjects::m_skybox, GameObjects::m_entities, GameObjects::m_particleEmitters, GameObjects::m_waterTiles, GameObjects::m_GUIs, GameObjects::m_texts, GameObjects::m_animatedModels, GameObjects::m_lines, GameObjects::m_clickables, GameObjects::m_lights);
	}

	void EnableCollision() {
		m_collision = !m_collision;
	}

protected:
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive & ar, const unsigned int version) const {
		ar << m_modelLoader;
		ar << m_textureLoader;
		ar << m_engine;
		ar << m_entities;
		ar << m_heightGenerators;
		ar << m_terrains;
		ar << m_skybox;
		ar << m_emitters;
		ar << m_waterTiles;
		ar << m_GUIs;
		ar << m_texts;
	}

	template<class Archive>
	void load(Archive & ar, const unsigned int version) {
		ar >> m_modelLoader;
		ar >> m_textureLoader;
		ar >> m_engine;
		ar >> m_entities;
		ar >> m_heightGenerators;
		ar >> m_terrains;
		ar >> m_skybox;
		ar >> m_emitters;
		ar >> m_waterTiles;
		ar >> m_GUIs;
		ar >> m_texts;
	}

	template<class Archive>
	void serialize(Archive& ar, const unsigned int file_version) {
		boost::serialization::split_member(ar, *this, file_version);
	}
};
#pragma once
#include "ShaderProgram.h"

/*Class used to manage the GUI shader*/
class GUIShader : public ShaderProgram {

private:
	GLint m_projectionMatrix;
	GLint m_modelMatrix;
	GLint m_affectedByLight;
	GLint m_lightDirection;
	GLint m_viewDirection;

public:
	GUIShader() : ShaderProgram(GUI_VERT_PATH, GUI_FRAG_PATH) {
		m_projectionMatrix = glGetUniformLocation(m_programID, "projectionMatrix");
		m_modelMatrix = glGetUniformLocation(m_programID, "modelMatrix");

		m_affectedByLight = glGetUniformLocation(m_programID, "affectedByLight");
		m_lightDirection = glGetUniformLocation(m_programID, "lightDirection");
		m_viewDirection = glGetUniformLocation(m_programID, "viewDirection");
	}

	void LoadProjectionMatrix(const Mat4& mat) { SetUniformMat4(m_projectionMatrix, mat); }
	void LoadModelMatrix(const Mat4& mat) { SetUniformMat4(m_modelMatrix, mat); }

	void LoadAffectedByLight(const float t_affected) { SetUniform1f(m_affectedByLight, t_affected); }
	void LoadLightDirection(const Vec3& t_direction) { SetUniformVec3(m_lightDirection, t_direction); }
	void LoadViewDirection(const Vec3& t_direction) { SetUniformVec3(m_viewDirection, t_direction); }
};
#include <stdlib.h>
#include <time.h>
#include <filesystem>
#include "Game.h"
#include "ModelLoader.h"
#include "TextureLoader.h"
#include "AnimationLoader.h"
#include "FontLoader.h"
#include "EntityDisplay.h"
#include "Scrollable.h"
#include "PhysicsEngine.h"
#include "BoundedCuboid.h"
#include "BoundedSphere.h"
#include "TerrainCollider.h"

namespace fs = std::filesystem;

void DrawTriangles(const std::vector<PhysicsEngine::Triangle>& triangles, std::shared_ptr<BoundedEntity> ent1, std::shared_ptr<BoundedEntity> ent2) {
	GameObjects::RemoveAllLines();
	if (triangles.size() == 0) return;
	float minPenetration = Vec3::Dot(triangles[0].normal, triangles[0].a);
	int index = 0;

	Vec3 smallest = minPenetration * triangles[0].normal;
	for (unsigned int i = 1; i < triangles.size(); ++i) {
		float penetration = Vec3::Dot(triangles[i].normal, triangles[i].a);
		if (penetration < minPenetration) {
			minPenetration = penetration;
			index = i;
			smallest = minPenetration * triangles[i].normal;
		}
	}
	std::shared_ptr<Line> smallVec = std::make_shared<Line>(Line(0, smallest, Vec4(1.0f, 0.0f, 0.0f, 1.0f), Vec4(0.0f, 0.0f, 1.0f, 1.0f)));
	GameObjects::Add(smallVec);

	for each (PhysicsEngine::Triangle triangle in triangles) {
		for each(std::pair<Vec3, Vec3> edge in triangle.GetEdges()) {
			std::shared_ptr<Line> line = std::make_shared<Line>(Line(edge.first, edge.second, Vec4(0.0f, 1.0f, 0.0f, 1.0f), Vec4(1.0f)));
			GameObjects::Add(line);
		}
	}

	Vec3 pos = ent1->GetPosition() - ent2->GetPosition();
	Vec3 scale = Vec3(2.0);
	Vec3 line1Pos = (pos - scale);
	line1Pos.x += scale.x * 2;

	Vec3 line2Pos = line1Pos;
	line2Pos.z += scale.z * 2;

	Vec3 line3Pos = pos - scale;
	line3Pos.z += scale.z * 2;

	std::shared_ptr<Line> line1 = std::make_shared<Line>(Line(pos - scale, line1Pos, Vec4(0.69f, 0.0f, 0.69f, 1.0f), Vec4(1.0f)));
	std::shared_ptr<Line> line2 = std::make_shared<Line>(Line(line1Pos, line2Pos, Vec4(0.69f, 0.0f, 0.69f, 1.0f), Vec4(1.0f)));
	std::shared_ptr<Line> line3 = std::make_shared<Line>(Line(line2Pos, line3Pos, Vec4(0.69f, 0.0f, 0.69f, 1.0f), Vec4(1.0f)));
	std::shared_ptr<Line> line4 = std::make_shared<Line>(Line(line3Pos, pos - scale, Vec4(0.69f, 0.0f, 0.69f, 1.0f), Vec4(1.0f)));

	Vec3 line1PosY = pos - scale;
	line1PosY.y += scale.y * 2;

	Vec3 line2PosY = line1Pos;
	line2PosY.y += scale.y * 2;

	Vec3 line3PosY = line2Pos;
	line3PosY.y += scale.y * 2;

	Vec3 line4PosY = line3Pos;
	line4PosY.y += scale.y * 2;

	std::shared_ptr<Line> line5 = std::make_shared<Line>(Line(pos - scale, line1PosY, Vec4(0.69f, 0.0f, 0.69f, 1.0f), Vec4(1.0f)));
	std::shared_ptr<Line> line6 = std::make_shared<Line>(Line(line1Pos, line2PosY, Vec4(0.69f, 0.0f, 0.69f, 1.0f), Vec4(1.0f)));
	std::shared_ptr<Line> line7 = std::make_shared<Line>(Line(line2Pos, line3PosY, Vec4(0.69f, 0.0f, 0.69f, 1.0f), Vec4(1.0f)));
	std::shared_ptr<Line> line8 = std::make_shared<Line>(Line(line3Pos, line4PosY, Vec4(0.69f, 0.0f, 0.69f, 1.0f), Vec4(1.0f)));

	std::shared_ptr<Line> line9 = std::make_shared<Line>(Line(line1PosY, line2PosY, Vec4(0.69f, 0.0f, 0.69f, 1.0f), Vec4(1.0f)));
	std::shared_ptr<Line> line10 = std::make_shared<Line>(Line(line2PosY, line3PosY, Vec4(0.69f, 0.0f, 0.69f, 1.0f), Vec4(1.0f)));
	std::shared_ptr<Line> line11 = std::make_shared<Line>(Line(line3PosY, line4PosY, Vec4(0.69f, 0.0f, 0.69f, 1.0f), Vec4(1.0f)));
	std::shared_ptr<Line> line12 = std::make_shared<Line>(Line(line4PosY, line1PosY, Vec4(0.69f, 0.0f, 0.69f, 1.0f), Vec4(1.0f)));
	
	GameObjects::Add(line1);
	GameObjects::Add(line2);
	GameObjects::Add(line3);
	GameObjects::Add(line4);
	GameObjects::Add(line5);
	GameObjects::Add(line6);
	GameObjects::Add(line7);
	GameObjects::Add(line8);
	GameObjects::Add(line9);
	GameObjects::Add(line10);
	GameObjects::Add(line11);
	GameObjects::Add(line12);
}

/*#include <socketapi.h>
#include <netioapi.h>
#include <stdio.h>
#include <websocket.h>
#include <windows.networking.sockets.h>
#include <stdlib.h>
#include <netiodef.h>
#include <netioapi.h>
#include <string.h>*/

int main() {
	//www.geeksforgeeks.org/socket-programming-cc/
	//stackoverflow.com/questions/14388706/socket-options-so-reuseaddr-and-so-reuseport-how-do-they-differ-do-they-mean-t?rq=1
	/*SOCKET sock = socket(AF_IPX, SOCK_DGRAM, IPPROTO_ICMP);
	if (sock == INVALID_SOCKET)
		wprintf(L"socket function failed with error = %d\n", WSAGetLastError());
	else {
		wprintf(L"socket function succeeded\n");

		// Close the socket to release the resources associated
		// Normally an application calls shutdown() before closesocket 
		//   to  disables sends or receives on a socket first
		// This isn't needed in this simple sample
		int iResult = closesocket(sock);
		if (iResult == SOCKET_ERROR) {
			wprintf(L"closesocket failed with error = %d\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}
	}
	char opt = 1;
	if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	WSACleanup();*/

	Mat4 perspective = Mat4::Perspective(PERSPECTIVE_FOV, PERSPECTIVE_ASPECT, PERSPECTIVE_NEAR, PERSPECTIVE_FAR);
	Mat4 orthographic = Mat4::Orthographic(ORTHOGRAPHIC_LEFT, ORTHOGRAPHIC_RIGHT, ORTHOGRAPHIC_BOTTOM, ORTHOGRAPHIC_TOP, ORTHOGRAPHIC_NEAR, ORTHOGRAPHIC_FAR);
	Camera camera = Camera(Vec3(0.0f, 0.0f, 0.0f), Vec3(0.54, -0.65, 0.51), 1.0f, 0.05f, Camera::FreeRoaming, Vec3(5.0f));
	Engine engine = Engine(perspective, orthographic, 1980, 1200, &camera);
	PhysicsEngine physicsEngine = PhysicsEngine();

	Game game = Game(engine, physicsEngine);

	Light sun1 = Light(Vec3(10000.0f, 10000.0f, 10000.0f), Vec3(1.0f), Vec3(1, 0, 0));
	Light sun2 = Light(Vec3(0.0f, 10000.0f, 0.0f), Vec3(0.0f), Vec3(1, 0, 0));
	Light sun3 = Light(Vec3(0.0f, 10000.0f, 0.0f), Vec3(0.0f), Vec3(1, 0, 0));
	Light sun4 = Light(Vec3(0.0f, 10000.0f, 0.0f), Vec3(0.0f), Vec3(1, 0, 0));

	//Button button = Button(BoundedGUI(GUI(TexturedModel(modelLoader.LoadModel("correctPlane"), Texture(game.m_engine.GetShadowMap(), 1, 0.01f, 1.0f), 0), "gui", Vec3(6.0f, 6.0f, -1.0f), Vec3(0.0f, 0.0f, 1.0f), Vec3(2.0f), "bounce"), BoundedRectangle(Vec2(0.0f), 1.0f, 1.0f)), std::function<void()>(testFunc));
	Skybox skybox = Skybox("Day", "Day", "Day", "Day", 0.001f);
	GameObjects::Add(std::make_shared<Skybox>(skybox));

	HeightGenerator heig = HeightGenerator(1337, 60.0f, 3, 32, 0.3f);

	Texture background = Texture(TextureLoader::LoadTexture("grass.png"), 0, 0.01f, 1.0f);
	Texture rPath = Texture(TextureLoader::LoadTexture("sand.png"), 0, 0.01f, 1.0f);
	Texture gPath = Texture(TextureLoader::LoadTexture("flowers.png"), 0, 0.01f, 1.0f);
	Texture bPath = Texture(TextureLoader::LoadTexture("path.png"), 0, 0.01f, 1.0f);
	Texture blend = Texture(TextureLoader::LoadTexture("blendMap.png"), 0, 0.01f, 1.0f);
	TerrainTexturePack pack = TerrainTexturePack(background, rPath, gPath, bPath);
	Terrain terrain1 = Terrain(heig, Vec2i(0), blend, pack, 128);
	Terrain terrain2 = Terrain(heig, Vec2i(0, -1), blend, pack, 128);
	Terrain terrain3 = Terrain(heig, Vec2i(-1, 0), blend, pack, 128);
	Terrain terrain4 = Terrain(heig, Vec2i(-1), blend, pack, 128);
	GameObjects::Add(std::make_shared<Terrain>(terrain1));
	GameObjects::Add(std::make_shared<Terrain>(terrain2));
	GameObjects::Add(std::make_shared<Terrain>(terrain3));
	GameObjects::Add(std::make_shared<Terrain>(terrain4));

	WaterTile waterTile1 = WaterTile("waterDUDV.png", "waterNormal.png", Vec3(400, WATER_HEIGHT, 400), Vec3(400.0f));
	WaterTile waterTile2 = WaterTile("waterDUDV.png", "waterNormal.png", Vec3(400, WATER_HEIGHT, -400), Vec3(400.0f));
	WaterTile waterTile3 = WaterTile("waterDUDV.png", "waterNormal.png", Vec3(-400, WATER_HEIGHT, 400), Vec3(400.0f));
	WaterTile waterTile4 = WaterTile("waterDUDV.png", "waterNormal.png", Vec3(-400, WATER_HEIGHT, -400), Vec3(400.0f));

	std::shared_ptr<ParticleEmitter> emitter = std::make_shared<ParticleEmitter>(ParticleEmitter(ParticleTexture(TextureLoader::LoadTexture("Particles\\cosmic.png"), 4, true), 10.0f, Vec3(250.0f, 4.0f, 203.0f), Vec3(0.0f, 10.0f, 0.0f), 0.01f, 5.0f, 10.0f));
	emitter->SetLifeLengthError(Vec2(-3.0f, 1.0f));
	emitter->SetVelocityErrorMin(Vec3(-4.0f));
	emitter->SetVelocityErrorMax(Vec3(4.0f));
	//GameObjects::Add(emitter);

	std::shared_ptr<ParticleEmitter> emitter2 = std::make_shared<ParticleEmitter>(ParticleEmitter(ParticleTexture(TextureLoader::LoadTexture("Particles\\fire.png"), 8, true), 50.0f, Vec3(300.0f, WATER_HEIGHT, 203.0f), Vec3(0.0f, 10.0f, 0.0f), 0.01f, 5.0f, 10.0f));
	emitter2->SetLifeLengthError(Vec2(-3.0f, 1.0f));
	emitter2->SetVelocityErrorMin(Vec3(-1.0f));
	emitter2->SetVelocityErrorMax(Vec3(1.0f));
	//GameObjects::Add(emitter2);

	AnimationLoader loader = AnimationLoader();
	std::shared_ptr<AnimatedModel> runMan;
	//GameObjects::Add(runMan = std::make_shared<AnimatedModel>(loader.LoadDAE("model.dae", "Character Texture.png")));
	//GameObjects::Add(std::make_shared<WaterTile>(waterTile1));
	//GameObjects::Add(std::make_shared<WaterTile>(waterTile2));
	//GameObjects::Add(std::make_shared<WaterTile>(waterTile3));
	//GameObjects::Add(std::make_shared<WaterTile>(waterTile4));
	
	GameObjects::Add(sun1);
	GameObjects::Add(sun2);
	GameObjects::Add(sun3);
	GameObjects::Add(sun4);

	std::shared_ptr<Line> line = std::make_shared<Line>(Line(Vec3(0.0f), Vec3(10.0f), Vec4(0.69f, 0.0f, 0.69f, 1.0f), Vec4(1.0f)));
	//GameObjects::Add(line);

	/*for (const fs::directory_entry& modelFile : fs::directory_iterator(MODEL_PATH)) {
		if (modelFile.is_directory()) continue;
		std::string modelPath = modelFile.path().u8string();
		modelPath = modelPath.substr(14, modelPath.length());
		ModelLoader::LoadModel(modelPath);
	}*/

	std::map<std::string, RawModel> models = ModelLoader::GetModels();
	std::shared_ptr<Scrollable> listOfObjects = std::make_shared<Scrollable>(Scrollable(Vec3(ORTHOGRAPHIC_LEFT + 1.0f, 0.0f, -100.0f), Vec3(4.0f, ORTHOGRAPHIC_TOP - ORTHOGRAPHIC_BOTTOM, 1.0f)));
	Vec2 position = Vec2(0.5f, ORTHOGRAPHIC_TOP - 1);

	/*for (std::pair<std::string, RawModel> pair : models) {
		std::string texturePath = pair.first.substr(0, pair.first.length() - 3);
		texturePath += "png";

		std::string normalPath = pair.first.substr(0, pair.first.length() - 4);
		normalPath += "Normal.png";

		GLuint texture = TextureLoader::LoadTexture(texturePath);
		if (texture == 0) texture = TextureLoader::LoadTexture("default.png");
		GLuint normalMap = TextureLoader::LoadTexture(normalPath);

		std::shared_ptr<Clickable> gui = std::make_shared<Clickable>(Clickable(BoundedGUI(GUI(TexturedModel(pair.second, Texture(texture, 1, 0.01f, 1.0f), 0), normalMap, "gui", Vec3(7.0f, 7.0f, -10.0f), Quaternion::Rotation(0.0f, Vec3(0.0f, 1.0f, 0.0f)), Vec3(0.1f), true, "bounce"), std::make_shared<BoundedRectangle>(BoundedRectangle(Vec2(7.0f, 7.0f), 1.0f, 1.0f))), std::function<void(Clickable*)>()));
		std::shared_ptr<EntityDisplay> display = std::make_shared<EntityDisplay>(EntityDisplay(Vec3(0.0f, 0.0f, -100.0f), gui, 1.0f));
		listOfObjects->AddControl(display, Vec3(position, 1.0f));

		if (position.x > 2.0f) {
			position.y -= 2.1f;
			position.x = 0.5f;
		} else {
			position += Vec2(2.1f, 0.0f);
		}
	}

	for (auto& textureFile : fs::directory_iterator(TEXTURE_PATH)) {
		if (textureFile.is_directory()) continue;
		TextureLoader::LoadTexture(textureFile.path().u8string());
	}*/
	
	GameObjects::Add((std::shared_ptr<Clickable>)listOfObjects);
	Vec3 pos = Vec3();

	std::shared_ptr<Entity> box1 = std::make_shared<Entity>(Entity(TexturedModel(ModelLoader::LoadModel("box.obj"), Texture(TextureLoader::LoadTexture("box.png"), 1, 0.1f, 0.1f), 0), 0, Vec3(1.0f, 30.0, 1.0f), Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vec3(1.0, 1.0, 1.0), Vec3(0.0, 0.0, 0.0), true, false, ""));
	std::shared_ptr<Entity> box2 = std::make_shared<Entity>(Entity(TexturedModel(ModelLoader::LoadModel("box.obj"), Texture(TextureLoader::LoadTexture("box.png"), 1, 0.1f, 0.1f), 0), 0, Vec3(10.0, 30.0, 1.0), Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vec3(1.0, 1.0, 1.0), Vec3(0.0, 0.0, 0.0), true, false, ""));
	GameObjects::Add(box1);
	GameObjects::Add(box2);

	//BoundedCuboid cub1 = BoundedCuboid(Vec3(1.0f, 30.0f, 1.0f), Vec3(2.0f, 2.0f, 2.0f));
	//BoundedCuboid cub2 = BoundedCuboid(Vec3(10.0f, 30.0f, 1.0f), Vec3(2.0f, 2.0f, 2.0f));
	
	std::shared_ptr<Entity> tester = std::make_shared<Entity>(Entity(TexturedModel(ModelLoader::LoadModel("sphere.obj"), Texture(TextureLoader::LoadTexture("box.png"), 1, 0.1f, 0.1f), 0), 0, Vec3(0.0, 60.0, 0.0), Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vec3(5.0, 5.0, 5.0), Vec3(0.0, 0.0, 0.0), true, false, ""));
	GameObjects::Add(tester);

	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			for (int k = 0; k < 3; ++k) {
				if (k % 2 == 0) {
					std::shared_ptr<BoundedSphere> cub = std::make_shared<BoundedSphere>(BoundedSphere(Vec3(20.0 - i * 3, 100.0 - k * 3, 20.0 - j * 3), Vec3(1.0f, 1.0f, 1.0f)));
					std::shared_ptr<BoundedEntity> ent = std::make_shared<BoundedEntity>(BoundedEntity(Entity(TexturedModel(ModelLoader::LoadModel("sphere.obj"), Texture(TextureLoader::LoadTexture("box.png"), 1, 0.1f, 0.1f), 0), 0, Vec3(20.0 - i * 3, 100.0 - k * 3, 20.0 - j * 3), Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vec3(1.0, 1.0, 1.0), Vec3(0.0, 0.0, 0.0), false, false, ""), cub));
					GameObjects::Add(ent);
				} else {
					std::shared_ptr<BoundedCuboid> cub = std::make_shared<BoundedCuboid>(BoundedCuboid(Vec3(20.0 - i * 3, 100.0 - k * 3, 20.0 - j * 3), Vec3(1.0f, 1.0f, 1.0f)));
					std::shared_ptr<BoundedEntity> ent = std::make_shared<BoundedEntity>(BoundedEntity(Entity(TexturedModel(ModelLoader::LoadModel("box.obj"), Texture(TextureLoader::LoadTexture("box.png"), 1, 0.1f, 0.1f), 0), 0, Vec3(20.0 - i * 3, 100.0 - k * 3, 20.0 - j * 3), Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vec3(1.0, 1.0, 1.0), Vec3(0.0, 0.0, 0.0), false, false, ""), cub));
					GameObjects::Add(ent);
				}
			}
		}
	}
	std::shared_ptr<BoundedCuboid> cub1 = std::make_shared<BoundedCuboid>(BoundedCuboid(Vec3(20.0f, 50.0f, 20.0f), Vec3(1.0f, 1.0f, 1.0f)));
	std::shared_ptr<BoundedSphere> cub2 = std::make_shared<BoundedSphere>(BoundedSphere(Vec3(10.0f, 30.0f, 1.0f), Vec3(1.0f, 1.0f, 1.0f)));
	std::shared_ptr<BoundedEntity> ent1 = std::make_shared<BoundedEntity>(BoundedEntity(Entity(TexturedModel(ModelLoader::LoadModel("box.obj"), Texture(TextureLoader::LoadTexture("box.png"), 1, 0.1f, 0.1f), 0), 0, Vec3(20.0, 50.0, 20.0), Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vec3(1.0, 1.0, 1.0), Vec3(0.0, 0.0, 0.0), false, false, ""), cub1));
	std::shared_ptr<BoundedEntity> ent2 = std::make_shared<BoundedEntity>(BoundedEntity(Entity(TexturedModel(ModelLoader::LoadModel("sphere.obj"), Texture(TextureLoader::LoadTexture("box.png"), 1, 0.1f, 0.1f), 0), 0, Vec3(10.0, 30.0, 1.0), Quaternion(1.0f, 0.0f, 0.0f, 0.0f), Vec3(1.0, 1.0, 1.0), Vec3(0.0, 0.0, 0.0), false, false, ""), cub2));
	GameObjects::Add(ent1);

	Vec3 contact = Vec3(0.0f);
	
	//GameObjects::Add(ent2); //TODO: can't add any more objects after adding BoundedEntity, they don't render, but their shadows do
	//also it seems to effect things if the objects have different raw models, the boundedENtity objects that is
	std::vector<Vec3> simplex = std::vector<Vec3>();
	simplex.push_back(Vec3(3.05645752, 1.78835964, 1.92720222));
	simplex.push_back(Vec3(-1.04519653, -2.17440510, -2.00627327));
	simplex.push_back(Vec3(-0.942314148, 1.88514996, 1.93181038));
	simplex.push_back(Vec3(1.05115509, 1.78621197, -2.06960106));

	ent1->SetPosition(Vec3(14.0000000, 18.4532223, 20.0000000));
	ent1->SetScale(Vec3(1.0, 1.0, 1.0));
	ent1->SetRotation(Quaternion(1.0, 0.0, 0.0, 0.0));
	//ent1->SetRotation(Quaternion(-0.121942118, 0.990881503, -0.0257345308, -0.0511591136));

	ent2->SetPosition(Vec3(14.0000000, 16.4587362, 20.0000000)); //TODO: check to see if it still bugs out with same positions
	ent2->SetScale(Vec3(1.0, 1.0, 1.0));
	ent2->SetRotation(Quaternion(1.0, 0.0, 0.0, 0.0));
	ent2->SetRotation(Quaternion(0.999588132, -0.0260854401, 0.000330955227, -0.0120920781));

	simplex = physicsEngine.TestGJK(contact, ent1->GetBoundingBody(), ent2->GetBoundingBody());

	std::vector<PhysicsEngine::Triangle> triangles = std::vector<PhysicsEngine::Triangle>();
	triangles.push_back(PhysicsEngine::Triangle(simplex[0], simplex[1], simplex[2]));
	triangles.push_back(PhysicsEngine::Triangle(simplex[0], simplex[1], simplex[3]));
	triangles.push_back(PhysicsEngine::Triangle(simplex[0], simplex[3], simplex[2]));
	triangles.push_back(PhysicsEngine::Triangle(simplex[1], simplex[2], simplex[3]));

	Vec3 smallest = Vec3(0.0f);

	while (!engine.ShouldStop()) {

		//runMan->SetPosition(pos);
		if (Input::GetKeyPress(GLFW_KEY_H)) {
			listOfObjects->SetVisible(!listOfObjects->GetVisible());
		}

		if (Input::GetKeyPress(GLFW_KEY_C)) {
			game.EnableCollision();
		}

		Vec3 pos = Vec3(0.0f);
		if (Input::GetKey(GLFW_KEY_I)) {
			pos.z -= 1.0f;
		} 
		if (Input::GetKey(GLFW_KEY_J)) {
			pos.x -= 1.0f;
		}
		if (Input::GetKey(GLFW_KEY_K)) {
			pos.z += 1.0f;
		}
		if (Input::GetKey(GLFW_KEY_L)) {
			pos.x += 1.0f;
		}
		if (Input::GetKey(GLFW_KEY_PERIOD)) {
			pos.y -= 1.0f;
		}
		if (Input::GetKey(GLFW_KEY_COMMA)) {
			pos.y += 1.0f;
		}
		
		ent1->SetPosition(ent1->GetPosition() + pos);
		if (Input::GetKeyPress(GLFW_KEY_0)) {
			smallest = physicsEngine.TestEPA(triangles, contact, simplex, ent1->GetBoundingBody(), ent2->GetBoundingBody());
		}
		
		if (Input::GetKeyPress(GLFW_KEY_R)) {
			triangles.clear();
			triangles.push_back(PhysicsEngine::Triangle(simplex[0], simplex[1], simplex[2]));
			triangles.push_back(PhysicsEngine::Triangle(simplex[0], simplex[1], simplex[3]));
			triangles.push_back(PhysicsEngine::Triangle(simplex[0], simplex[3], simplex[2]));
			triangles.push_back(PhysicsEngine::Triangle(simplex[1], simplex[2], simplex[3]));
		}

		DrawTriangles(triangles, ent1, ent2);

		game.PushToEngine();
	}

	engine.Stop();

	return 0;
}
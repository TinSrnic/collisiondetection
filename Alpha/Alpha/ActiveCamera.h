#pragma once
#include "Camera.h"

class ActiveCamera {

	friend class Engine;
private:
	static Camera* m_camera;

public:
	static Camera* GetActiveCamera() { return m_camera; }

};
#pragma once
#include "RawModel.h"
#include "Texture.h"
#include "Math.h"

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/list.hpp>

/*Class used to store data of a RawModel and Texture 
as well as a texture index specifying the position of the texture in the atlas*/
class TexturedModel {

private:
	RawModel m_rawModel;
	Texture m_texture;
	unsigned int m_textureIndex;

public:
	TexturedModel(const RawModel& t_model, const Texture& t_texture, unsigned int t_textureIndex) 
		: m_rawModel(t_model), m_texture(t_texture), m_textureIndex(t_textureIndex) { }

	const RawModel& GetRawModel() const { return m_rawModel; }
	const Texture& GetTexture() const { return m_texture; }

	//gets offset to correct image in atlas
	Vec2 GetAtlasOffset() const {
		unsigned int numOfRows = m_texture.GetNumberOfRows();
		unsigned int column = m_textureIndex % numOfRows;
		unsigned int row = m_textureIndex / numOfRows;
		return Vec2((float)column / (float)numOfRows, (float)row / (float)numOfRows);
	}

	//serialization
protected:
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive & ar, const unsigned int version) const {
		// invoke serialization of the base class 
		ar << boost::serialization::base_object<RawModel>(*this);
		ar << boost::serialization::base_object<Texture>(*this);
	}

	template<class Archive>
	void load(Archive & ar, const unsigned int version) {
		// invoke serialization of the base class 
		ar >> boost::serialization::base_object<RawModel>(*this);
		ar >> boost::serialization::base_object<Texture>(*this);
	}

	template<class Archive>
	void serialize(Archive& ar, const unsigned int file_version) {
		boost::serialization::split_member(ar, *this, file_version);
	}
};
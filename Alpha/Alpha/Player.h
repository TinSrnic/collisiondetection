#pragma once
#include "Entity.h"
#include "Terrain.h"
#define MOVMENT_SPEED 2.0f
#define ROTATION_SPEED 10

class Player : public Entity {

private:
	Window& m_window;
	Vec3 m_viewDirection;
	Vec3 m_currentView;
	bool m_isInAir = false;
	float m_rotationAngle = 0;

public:
	Player(Window& window, AudioMaster& audioMaster, ALuint sound, RawModel t_rawModel, Texture t_texture) 
	: m_window(window), Entity(audioMaster, sound, t_rawModel, t_texture, 0, false, Vec3(0.0f, 0.0f, 0.0f)) {
		m_viewDirection = Vec3(0.0, 0.0, -1.0);
		m_currentView = Vec3(0.0, 0.0, -1.0);
	}

	void UpdatePlayer(Terrain& terrain) {
		Vec3 terrainLevelView = Vec3(m_currentView.x, 0.0f, m_currentView.z);
		if (m_window.m_keys[GLFW_KEY_W]) { m_position += terrainLevelView * MOVMENT_SPEED; }
		if (m_window.m_keys[GLFW_KEY_D]) {
			Mat4 rot = Mat4::Rotation(-ROTATION_SPEED, Vec3(0.0f, 1.0f, 0.0f));
			m_currentView = rot * m_currentView;
			m_rotationAngle -= ROTATION_SPEED;
		}
		if (m_window.m_keys[GLFW_KEY_S]) { m_position -= terrainLevelView * MOVMENT_SPEED; }
		if (m_window.m_keys[GLFW_KEY_A]) {
			Mat4 rot = Mat4::Rotation(ROTATION_SPEED, Vec3(0.0f, 1.0f, 0.0f));
			m_currentView = rot * m_currentView;
			m_rotationAngle += ROTATION_SPEED;
		} if (!m_isInAir && m_window.m_keys[GLFW_KEY_SPACE]) {
		}

		m_position.y = terrain.GetHeight(Vec2(m_position.x, m_position.z));
		m_modelMatrix = Mat4::Translation(m_position) * Mat4::Rotation(m_rotationAngle, Vec3(0.0f, 1.0f, 0.0f));
	}	
};
#include "GameObjects.h"
#include "Entity.h"
#include "Terrain.h"
#include "Skybox.h"
#include "ParticleEmitter.h"
#include "WaterTile.h"
#include "Clickable.h"
#include "Text.h"
#include "AnimatedModel.h"
#include "Line.h"
#include "Light.h"
#include "BoundedEntity.h"

#include "ModelLoader.h"
#include "TextureLoader.h"

std::vector<Light> GameObjects::m_lights = std::vector<Light>();
std::map<GLuint, std::list<std::shared_ptr<Entity>>> GameObjects::m_entities = std::map<GLuint, std::list<std::shared_ptr<Entity>>>();
std::list<std::shared_ptr<Terrain>> GameObjects::m_terrains = std::list<std::shared_ptr<Terrain>>();
std::shared_ptr<Skybox> GameObjects::m_skybox;
std::map<GLuint, std::list<std::shared_ptr<ParticleEmitter>>> GameObjects::m_particleEmitters = std::map<GLuint, std::list<std::shared_ptr<ParticleEmitter>>>();
std::list<std::shared_ptr<WaterTile>> GameObjects::m_waterTiles = std::list<std::shared_ptr<WaterTile>>();
std::map<GLuint, std::vector<std::shared_ptr<GUI>>> GameObjects::m_GUIs = std::map<GLuint, std::vector<std::shared_ptr<GUI>>>();
std::list<std::shared_ptr<Clickable>> GameObjects::m_clickables = std::list<std::shared_ptr<Clickable>>();
std::map<GLuint, std::vector<std::shared_ptr<Text>>> GameObjects::m_texts = std::map<GLuint, std::vector<std::shared_ptr<Text>>>();
std::map<GLuint, std::vector<std::shared_ptr<AnimatedModel>>> GameObjects::m_animatedModels = std::map<GLuint, std::vector<std::shared_ptr<AnimatedModel>>>();
std::vector<std::shared_ptr<Line>> GameObjects::m_lines = std::vector<std::shared_ptr<Line>>();
std::vector<std::shared_ptr<BoundedEntity>> GameObjects::m_boundedEntities = std::vector<std::shared_ptr<BoundedEntity>>();

void GameObjects::Add(const Light& light) {
	m_lights.push_back(light);
}
void GameObjects::Add(const std::shared_ptr<Entity>& t_entity) {
	try {
		m_entities[t_entity->GetTexture().GetTextureID()].push_back(t_entity);
	} catch (std::out_of_range) {
		m_entities.insert(std::pair<GLuint, std::list<std::shared_ptr<Entity>>>
			(t_entity->GetTexture().GetTextureID(), std::list<std::shared_ptr<Entity>>({ t_entity })));
	}
}
void GameObjects::Add(const std::shared_ptr<Terrain>& t_terrain) {
	m_terrains.push_back(t_terrain);
}
void GameObjects::Add(const std::shared_ptr<Skybox>& t_skybox) {
	m_skybox = t_skybox;
}
void GameObjects::Add(const std::shared_ptr<ParticleEmitter>& t_particleEmitter) {
	try {
		m_particleEmitters[t_particleEmitter->GetTexture().GetTextureID()].push_back(t_particleEmitter);
	} catch (std::out_of_range) {
		m_particleEmitters.insert(std::pair<GLuint, std::list<std::shared_ptr<ParticleEmitter>>>
			(t_particleEmitter->GetTexture().GetTextureID(), std::list<std::shared_ptr<ParticleEmitter>>({ t_particleEmitter })));
	}
}
void GameObjects::Add(const std::shared_ptr<WaterTile>& t_waterTile) {
	m_waterTiles.push_back(t_waterTile);
}
void GameObjects::Add(const std::shared_ptr<GUI>& t_gui) {
	GLuint textureID = t_gui->GetTexture().GetTextureID();
	try {
		m_GUIs[textureID].push_back(t_gui);
	} catch (std::out_of_range) {
		m_GUIs.insert(std::pair<GLuint, std::vector<std::shared_ptr<GUI>>>
			(textureID, std::vector<std::shared_ptr<GUI>>({ t_gui })));
	}
	
	std::sort(m_GUIs.at(textureID).begin(), m_GUIs.at(textureID).end(), [](const std::shared_ptr<GUI>& a, const std::shared_ptr<GUI>& b) -> bool {
		return a->GetPosition().z < b->GetPosition().z;
	});

	t_gui->AddedToGame();
}
void GameObjects::Add(const std::shared_ptr<Clickable>& t_clickable) {
	try {
		m_GUIs[t_clickable->GetTexture().GetTextureID()].push_back(t_clickable);
	} catch (std::out_of_range) {
		m_GUIs.insert(std::pair<GLuint, std::vector<std::shared_ptr<GUI>>>
			(t_clickable->GetTexture().GetTextureID(), std::vector<std::shared_ptr<GUI>>({ t_clickable })));
	}

	m_clickables.push_back(t_clickable);
	t_clickable->AddedToGame();
}
void GameObjects::Add(const std::shared_ptr<Text>& t_text) {
	try {
		m_texts[t_text->GetFont().GetTextureID()].push_back(t_text);
	} catch (std::out_of_range) {
		m_texts.insert(std::pair<GLuint, std::vector<std::shared_ptr<Text>>>
			(t_text->GetFont().GetTextureID(), std::vector<std::shared_ptr<Text>>({ t_text })));
	}
}
void GameObjects::Add(const std::shared_ptr<AnimatedModel>& t_animatedModel) {
	try {
		m_animatedModels[t_animatedModel->GetTextureID()].push_back(t_animatedModel);
	} catch (std::out_of_range) {
		m_animatedModels.insert(std::pair<GLuint, std::vector<std::shared_ptr<AnimatedModel>>>
			(t_animatedModel->GetTextureID(), std::vector<std::shared_ptr<AnimatedModel>>({ t_animatedModel })));
	}
}
void GameObjects::Add(const std::shared_ptr<Line>& t_line) {
	m_lines.push_back(t_line);
}
void GameObjects::Add(const std::shared_ptr<BoundedEntity>& t_boundedEntity) {
	m_boundedEntities.push_back(t_boundedEntity);
	GameObjects::Add((std::shared_ptr<Entity>)t_boundedEntity);
}

void GameObjects::Remove(const std::shared_ptr<Line>& t_line) {
	m_lines.erase(std::remove(m_lines.begin(), m_lines.end(), t_line), m_lines.end());
}
void GameObjects::RemoveAllLines() {
	m_lines.clear();
}

int GameObjects::GetVertexCount() {
	int numOfVertices = 0;
	for each(auto pair in m_entities) {
		for each(auto entity in pair.second) {
			numOfVertices += entity->GetRawModel().GetVertexCount();
		}
	}
	return numOfVertices;
}

float GameObjects::GetTerrainHeight(const Vec2& t_position) {
	float height = 0;
	
	for (std::shared_ptr<Terrain> terrain : m_terrains) {
		float tempHeight = terrain->GetHeight(t_position);
		if (tempHeight > height) height = tempHeight;
	}

	return height;
}
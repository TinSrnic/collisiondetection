#pragma once
#include "Entity.h"
#include "BoundedBody.h"
#include "Math.h"

class BoundedEntity : public Entity {

private:
	Vec3 m_angularVelocity;
	Quaternion m_rotation;
	std::shared_ptr<BoundedBody> m_boundingBody;

public:
	BoundedEntity(const Entity& t_entity, const std::shared_ptr<BoundedBody>& t_boundingBody) : Entity(t_entity) {
		m_boundingBody = t_boundingBody;
		m_angularVelocity = Vec3(0.0f);
		m_rotation = Quaternion(1.0f, 0.0f, 0.0f, 0.0f);
	}

	Vec3 GetPosition() { return Entity::GetPosition(); }
	std::shared_ptr<BoundedBody> GetBoundingBody() { return m_boundingBody; }
	Vec3 GetAngularVelocity() { return m_angularVelocity; }

	void SetAngularVelocity(const Vec3& t_angularVeloctity) {
		m_angularVelocity = t_angularVeloctity;
	}
	void SetPosition(const Vec3& t_position) {
		Entity::SetPosition(t_position);
		m_boundingBody->SetPosition(t_position);
	}
	void SetRotation(const Quaternion& t_rotation) {
		Quaternion norm = Quaternion::Normalize(t_rotation);
		Entity::SetRotation(norm);
		m_boundingBody->SetRotation(norm);
		m_rotation = norm;
	}

	void AddRotation(const Vec3& t_rotation) {
		//en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
		double cy = cos(t_rotation.z * 0.5);
		double sy = sin(t_rotation.z * 0.5);
		double cp = cos(t_rotation.y * 0.5);
		double sp = sin(t_rotation.y * 0.5);
		double cr = cos(t_rotation.x * 0.5);
		double sr = sin(t_rotation.x * 0.5);

		Quaternion q = Quaternion();
		q.w = cy * cp * cr + sy * sp * sr;
		q.x = cy * cp * sr - sy * sp * cr;
		q.y = sy * cp * sr + cy * sp * cr;
		q.z = sy * cp * cr - cy * sp * sr;

		m_rotation = q * m_rotation;
		Entity::SetRotation(m_rotation);
		m_boundingBody->SetRotation(m_rotation);
	}
};
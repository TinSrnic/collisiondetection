#pragma once
#include <map>
#include <list>
#include <vector>
#include <memory>
#include <sstream>
#include <glew.h>
#include "Math.h"

class Entity; 
class Terrain;
class Skybox;
class ParticleEmitter;
class WaterTile;
class GUI;
class Clickable;
class Text;
class Font;
class AnimatedModel;
class Line;
class BoundedEntity;

class Light;


class GameObjects {
	
	friend class Game;
private:
	static std::vector<Light> m_lights;
	 
	//all objects
	static std::map<GLuint, std::list<std::shared_ptr<Entity>>> m_entities;
	static std::list<std::shared_ptr<Terrain>> m_terrains;
	static std::shared_ptr<Skybox> m_skybox;
	static std::map<GLuint, std::list<std::shared_ptr<ParticleEmitter>>> m_particleEmitters;
	static std::list<std::shared_ptr<WaterTile>> m_waterTiles;
	static std::map<GLuint, std::vector<std::shared_ptr<GUI>>> m_GUIs;
	static std::list<std::shared_ptr<Clickable>> m_clickables;
	static std::map<GLuint, std::vector<std::shared_ptr<Text>>> m_texts;
	static std::map<GLuint, std::vector<std::shared_ptr<AnimatedModel>>> m_animatedModels;
	static std::vector<std::shared_ptr<Line>> m_lines;
	static std::vector<std::shared_ptr<BoundedEntity>> m_boundedEntities;

public:
	static void Add(const Light& light);
	static void Add(const std::shared_ptr<Entity>& t_entity);
	static void Add(const std::shared_ptr<Terrain>& t_terrain);
	static void Add(const std::shared_ptr<Skybox>& t_skybox);
	static void Add(const std::shared_ptr<ParticleEmitter>& t_particleEmitter);
	static void Add(const std::shared_ptr<WaterTile>& t_waterTile);
	static void Add(const std::shared_ptr<GUI>& t_gui);
	static void Add(const std::shared_ptr<Clickable>& t_clickable);
	static void Add(const std::shared_ptr<Text>& t_text);
	static void Add(const std::shared_ptr<AnimatedModel>& t_animatedModel);
	static void Add(const std::shared_ptr<Line>& t_line);
	static void Add(const std::shared_ptr<BoundedEntity>& t_boundedEntity);

	static void Remove(const std::shared_ptr<Line>& t_line);
	static void RemoveAllLines();

	static int GetVertexCount();
	static float GetTerrainHeight(const Vec2& t_position);
};
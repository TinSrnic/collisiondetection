#pragma once
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include <boost/serialization/serialization.hpp>

/* Class used to store data about an opengl texture as well as some extra info such as reflectivity and shine dampening used in the shader.
It also holds the number of rows a given texture has.*/
class Texture {

protected:
	GLuint m_textureID;
	unsigned int m_numberOfRows;

	float m_reflectivity;
	float m_shineDampening;

public:
	Texture(GLuint t_texID, unsigned int t_numberOfRows, float t_reflectivity, float t_shineDampening) 
	: m_textureID(t_texID), m_numberOfRows(t_numberOfRows), m_reflectivity(t_reflectivity), m_shineDampening(t_shineDampening) { }

	GLuint GetTextureID() const { return m_textureID; }
	unsigned int GetNumberOfRows() const { return m_numberOfRows; }

	float GetReflectivity() const { return m_reflectivity; }
	float GetShineDampening() const { return m_shineDampening; }

	void SetReflectivity(float t_reflectivity) { m_reflectivity = t_reflectivity; }
	void SetShineDampening(float t_shineDampening) { m_shineDampening = t_shineDampening; }

	//serialization
protected:
	friend class boost::serialization::access;

	template<class Archive>
	void save(Archive& ar, const unsigned int version) const {
		// invoke serialization of the base class 
		ar << m_textureID;
		ar << m_numberOfRows;
		ar << m_reflectivity;
		ar << m_hasTransparency;
		ar << m_useFakeLighting;
	}

	template<class Archive>
	void load(Archive& ar, const unsigned int version) {
		// invoke serialization of the base class 
		ar >> m_textureID;
		ar >> m_numberOfRows;
		ar >> m_reflectivity;
		ar >> m_hasTransparency;
		ar >> m_useFakeLighting;
	}

	template<class Archive>
	void serialize(Archive& ar, const unsigned int file_version) {
		boost::serialization::split_member(ar, *this, file_version);
	}
};
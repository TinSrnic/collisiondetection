#pragma once
#include <iostream>
#include <string>
#include <al.h>

struct RIFF_Header {
	char chunkID[4];
	long chunkSize;//size not including chunkSize or chunkID
	char format[4];
};

/*
* Struct to hold fmt subchunk data for WAVE files.
*/
struct WAVE_Format {
	char subChunkID[4];
	long subChunkSize;
	short audioFormat;
	short numChannels;
	long sampleRate;
	long byteRate;
	short blockAlign;
	short bitsPerSample;
};

/*
* Struct to hold the data of the wave file
*/
struct WAVE_Data {
	char subChunkID[4]; //should contain the word data
	long subChunk2Size; //Stores the size of the data block
};

void loadWavFile(std::string filename, unsigned char*& data, ALsizei& size, ALsizei& frequency, ALenum& format) {
	//Local Declarations
	FILE* soundFile = NULL;
	WAVE_Format wave_format;
	RIFF_Header riff_header;
	WAVE_Data wave_data;
	data = nullptr;

	soundFile = fopen(filename.c_str(), "rb");
	if (!soundFile) std::cerr << "Could not open file!" << std::endl;

	fread(&riff_header, sizeof(RIFF_Header), 1, soundFile);
	if ((riff_header.chunkID[0] != 'R' || riff_header.chunkID[1] != 'I' ||
		riff_header.chunkID[2] != 'F' || riff_header.chunkID[3] != 'F') ||
		(riff_header.format[0] != 'W' || riff_header.format[1] != 'A' ||
			riff_header.format[2] != 'V' || riff_header.format[3] != 'E'))
		throw ("Invalid RIFF or WAVE Header");

	fread(&wave_format, sizeof(WAVE_Format), 1, soundFile);
	//check for fmt tag in memory
	if (wave_format.subChunkID[0] != 'f' ||
		wave_format.subChunkID[1] != 'm' ||
		wave_format.subChunkID[2] != 't' ||
		wave_format.subChunkID[3] != ' ')
		throw ("Invalid Wave Format");

	//check for extra parameters;
	if (wave_format.subChunkSize > 16) fseek(soundFile, sizeof(short), SEEK_CUR);

	//Read in the the last byte of data before the sound file
	fread(&wave_data, sizeof(WAVE_Data), 1, soundFile);
	//check for data tag in memory
	if (wave_data.subChunkID[0] != 'd' ||
		wave_data.subChunkID[1] != 'a' ||
		wave_data.subChunkID[2] != 't' ||
		wave_data.subChunkID[3] != 'a')
		throw ("Invalid data header");


	//Allocate memory for data and pass other parameters
	data = new unsigned char[wave_data.subChunk2Size];
	size = wave_data.subChunk2Size;
	frequency = wave_format.sampleRate;
	if (wave_format.numChannels == 1) {
		if (wave_format.bitsPerSample == 8) format = AL_FORMAT_MONO8;
		else if (wave_format.bitsPerSample == 16) format = AL_FORMAT_MONO16;
	} else if (wave_format.numChannels == 2) {
		if (wave_format.bitsPerSample == 8) format = AL_FORMAT_STEREO8;
		else if (wave_format.bitsPerSample == 16) format = AL_FORMAT_STEREO16;
	}

	// Read in the sound data into the soundData variable
	if (!fread(data, wave_data.subChunk2Size, 1, soundFile)) throw ("error loading WAVE data into struct!");

	fclose(soundFile);
}

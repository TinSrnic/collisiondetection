#pragma once
#include "ProcessInfo.h"
#include "MasterRenderer.h"
#include "ParticleMaster.h"
#include "WaterMaster.h"
#include "GUIMaster.h"
#include "TextMaster.h"
#include "ShadowMapMaster.h"
#include "AnimationMaster.h"
#include "LineMaster.h"
#include "FPSCounter.h"
#include "AudioMaster.h"
#include "MousePickerOrthographic.h"
#include "MousePickerPerspective.h"
#include "Time.h"
#include "Button.h"
#include "Clickable.h"
#include "ActiveCamera.h"
#include "FontLoader.h"
#include "StringManipulation.h"
#include "GameObjects.h"

class Engine {
	
public:
	GLuint GetShadowMap() { return m_shadowMapMaster.GetShadowMap(); }

private:
	Window m_window;
public:
	Camera* m_camera;
private:
	bool m_displayInfo = false;
	bool m_mouseUnlock = false;

	WaterFBO m_waterFBO;

	MasterRenderer m_masterRenderer;
	ParticleMaster m_particleMaster;
	WaterMaster m_waterMaster;
	GUIMaster m_guiMaster;
	TextMaster m_textMaster;
	AnimationMaster m_animationMaster;
	LineMaster m_lineMaster;

public:
	ShadowMapMaster m_shadowMapMaster;
private:
	AudioMaster m_audioMaster = AudioMaster();

	Mat4 m_perspectiveMatrix;
	Mat4 m_orthographicMatrix;

	ProcessInfo m_process = ProcessInfo();
	FPSCounter m_fps = FPSCounter(1.0);

	int m_numberOfParticles = 0;
	Font font = FontLoader::LoadFont("candara.fnt", "candara.png");
	Text m_screenInfo = Text(font, "Screen width: Screen height: FPS: ", Vec3(0, 16.0f, 0));
	Text m_camPos = Text(font, "Camera position: ", Vec3(0, 15.5f, 0));
	Text m_camView = Text(font, "Camera view direction: ", Vec3(0, 15.0f, 0));
	Text m_textMousePerspective = Text(font, "Mouse: ", Vec3(0, 14.5f, 0.0f));
	Text m_lookText = Text(font, "Do I sees it", Vec3(0, 14.0f, 0.0f));
	Text m_textVirtualMemTotal = Text(font, "Total virtual memory: ", Vec3(0, 13.0f, 0));
	Text m_textVirtualMemUsed = Text(font, "Virtual memory used: ", Vec3(0, 12.5f, 0));
	Text m_textVirtualMemProcess = Text(font, "Virtual memory used by process: ", Vec3(0, 12.0f, 0));
	Text m_textPhysicalMemTotal = Text(font, "Physical memory used: ", Vec3(0, 11.0f, 0));
	Text m_textPhysicalMemUsed = Text(font, "Physical memory used by process: ", Vec3(0, 10.5f, 0));
	Text m_textPhysicalMemProcess = Text(font, "Physical memory used by process: ", Vec3(0, 10.0f, 0));
	Text m_textCPUTotal = Text(font, "Total CPU used: ", Vec3(0, 9.0f, 0));
	Text m_textCPUProcess = Text(font, "CPU used by process: ", Vec3(0, 8.5f, 0));
	Text m_particlesOnScreen = Text(font, "Particles: ", Vec3(0, 15.0f, 0.0));
	std::map<GLuint, std::vector<std::shared_ptr<Text>>> m_metadataMap = std::map<GLuint, std::vector<std::shared_ptr<Text>>>();

public:
	Engine(const Mat4& t_perspectiveMatrix, const Mat4& t_orthographicMatrix, unsigned int t_screenWidth, unsigned int t_screenHeight, Camera* t_camera)
		: m_window(t_screenWidth, t_screenHeight), m_camera(t_camera), m_waterFBO(m_window), m_masterRenderer(t_perspectiveMatrix), m_particleMaster(m_window, t_perspectiveMatrix),
		m_waterMaster(m_window, t_perspectiveMatrix), m_guiMaster(t_orthographicMatrix), m_textMaster(t_orthographicMatrix),
		m_shadowMapMaster(m_window, *m_camera), m_animationMaster(t_perspectiveMatrix), m_lineMaster(t_perspectiveMatrix) {

		World::m_orthographicMatrix = t_orthographicMatrix;
		World::m_perspectiveMatrix = t_perspectiveMatrix;
		ActiveCamera::m_camera = t_camera;

		srand((unsigned int)time(NULL));
		//gl variable setup
		glEnable(GL_DEPTH_TEST); //to acctually be able to render 3d stuff
		glEnable(GL_CULL_FACE); //speed increase don't render the back of things which usually can't be seen
		glCullFace(GL_BACK);
		glClearColor(0.5f, 0.5f, 0.5f, 1.0);
		

		//openAL setup for exponential decrease in sound loudness
		alDistanceModel(AL_EXPONENT_DISTANCE);

		m_perspectiveMatrix = t_perspectiveMatrix;
		m_orthographicMatrix = t_orthographicMatrix;
	}

	bool ShouldStop() { return m_window.ShouldWindowClose(); }
	void Render(std::list<std::shared_ptr<Terrain>>& t_terrains, std::shared_ptr<Skybox>& t_skybox, std::map<GLuint, std::list<std::shared_ptr<Entity>>>& t_entities,
		std::map<GLuint, std::list<std::shared_ptr<ParticleEmitter>>>& t_particleEmitters, std::list<std::shared_ptr<WaterTile>> t_waterTiles, 
		std::map<GLuint, std::vector<std::shared_ptr<GUI>>> t_GUIs, std::map<GLuint, std::vector<std::shared_ptr<Text>>> t_texts,
		std::map<GLuint, std::vector<std::shared_ptr<AnimatedModel>>> t_animatedModels, std::vector<std::shared_ptr<Line>>& t_lines, 
		std::list<std::shared_ptr<Clickable>> t_clickables, const std::vector<Light>& t_lights) {
		
		UpdateTime();
		UpdateCamera();
		UpdateMetadata();
		m_window.Clear();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		MousePickerOrthographic::Update();
		MousePickerPerspective::Update(m_camera->GetViewMatrix());
		m_numberOfParticles = 0;
		UpdateParticles(t_particleEmitters);
		UpdateEntities(t_entities);
		UpdateGUIs(t_GUIs);
		UpdateClickables(t_clickables);
		UpdateShadows(t_terrains, t_entities, t_lights.at(0));
		UpdateWater(t_terrains, t_skybox, t_entities, t_particleEmitters, t_lights);
		m_process.Update();

		//render
		m_masterRenderer.Render(t_terrains, t_skybox, t_entities, t_lights, *m_camera, m_shadowMapMaster.GetToShadowMapSpaceMatrix(), m_shadowMapMaster.GetShadowMap(), m_window.GetTime());
		m_waterMaster.Render(t_waterTiles, m_waterFBO, *m_camera, t_lights[0]);
		m_particleMaster.Render(t_particleEmitters, *m_camera);
		m_animationMaster.Render(t_animatedModels, *m_camera, t_lights, m_window.GetFrameTime());
		m_lineMaster.Render(t_lines, m_camera->GetViewMatrix());
		m_textMaster.Render(t_texts);
		m_guiMaster.Render(t_GUIs, t_lights.at(0));
		m_textMaster.Render(m_metadataMap);

		
		m_window.Update();
	}
	void Stop() {
		m_masterRenderer.CleanUp();
		m_particleMaster.CleanUp();
		m_waterMaster.CleanUp();
		m_guiMaster.CleanUp();
		m_textMaster.CleanUp();
		m_shadowMapMaster.CleanUp();
		m_audioMaster.CleanUp();

		glfwTerminate();
	}

	float GetFrameTime() { return m_window.GetFrameTime(); }
	float GetTime() { return m_window.GetTime(); }

	void SetCamera(Camera* t_camera) { 
		m_camera = t_camera; 
		ActiveCamera::m_camera = t_camera;
	}

	void EnableClipping() { glEnable(GL_CLIP_DISTANCE0); }
	void DisableClipping() { glDisable(GL_CLIP_DISTANCE0); }
	void LoadClipPlane(const Vec4& t_clipPlane) { 
		m_masterRenderer.LoadClipPlane(t_clipPlane);
	}

	void InvertCamera() {
 		Vec3 cameraPosition = m_camera->GetPosition();
		float cameraHeight = WATER_HEIGHT - cameraPosition.y;
		m_camera->SetPosition(Vec3(cameraPosition.x, cameraHeight, cameraPosition.z));
		Vec3 viewPleasure = m_camera->GetViewDirection();
		viewPleasure.y = -viewPleasure.y;
		m_camera->SetViewDirection(viewPleasure);
	}

private:
	void UpdateClickables(std::list<std::shared_ptr<Clickable>> t_clickables) {
		Vec2 mousePos = MousePickerOrthographic::GetCurrentRay();
		if (Input::GetMouseDown(GLFW_MOUSE_BUTTON_LEFT)) {
			for each(std::shared_ptr<Clickable> clickable in t_clickables) {
				if (clickable->GetMouseHeld()) clickable->OnMouseHeld();
				if (clickable->IsInObject(mousePos)) {
					if (!clickable->GetMouseHeld()) {
						clickable->OnMouseClick();
						if (!clickable->GetMouseInObject()) clickable->OnMouseEnter();
					}
					
				}
			}
		} else {
			for each(std::shared_ptr<Clickable> clickable in t_clickables) {
				if (!clickable->IsInObject(mousePos)) {
					if(clickable->GetMouseInObject()) clickable->OnMouseLeave();
				}
				if (clickable->GetMouseHeld()) clickable->OnMouseRelease();
			}
		}
	}
	
	void UpdateTime() {
		Time::m_time = m_window.GetTime();
		Time::m_deltaTime = m_window.GetFrameTime();
		m_fps.Time();
		Time::m_frameCount = m_fps.GetFrames();
	}
	void UpdateCamera() {
		//update view
		float mouseMoveX;
		float mouseMoveY;

		if (!m_mouseUnlock) {
			mouseMoveX = (float)((m_window.m_cursorXpos - m_window.m_width / 2.0f)); //get how much you should move to the right or left
			mouseMoveY = (float)((m_window.m_cursorYpos - m_window.m_height / 2.0f)); //get how much you should move up or down
			if (abs(mouseMoveX) < DEAD_ZONE) mouseMoveX = 0.0f;
			if (abs(mouseMoveY) < DEAD_ZONE) mouseMoveY = 0.0f;
		} else {
			mouseMoveX = 0.0f;
			mouseMoveY = 0.0f;
		}

		//update position
		Vec3 up = Vec3(0.0f, 1.0f, 0.0f);
		Vec3 move = Vec3(0.0f);
		Vec3 cameraView = m_camera->GetViewDirection();
		if (m_window.m_keys[GLFW_KEY_SPACE]) {
			move += up;
		}
		if (m_window.m_keys[GLFW_KEY_LEFT_SHIFT]) {
			move -= up;
		}
		if (m_window.m_keys[GLFW_KEY_W]) {
			move += cameraView;
		}
		if (m_window.m_keys[GLFW_KEY_A]) {
			move -= Vec3::Normalize(Vec3::Cross(cameraView, up));
		}
		if (m_window.m_keys[GLFW_KEY_S]) {
			move -= cameraView;
		}
		if (m_window.m_keys[GLFW_KEY_D]) {
			move += Vec3::Normalize(Vec3::Cross(cameraView, up));
		}

		m_camera->UpdateCamera(mouseMoveX, mouseMoveY, move);
	}
	void UpdateParticles(std::map<GLuint, std::list<std::shared_ptr<ParticleEmitter>>> t_particleEmitters) {
		for each (std::pair<GLuint, std::list<std::shared_ptr<ParticleEmitter>>> pair in t_particleEmitters) {
			for each(std::shared_ptr<ParticleEmitter> emitter in pair.second) {
				emitter->Update(m_camera->GetPosition(), m_window.GetFrameTime());
				m_numberOfParticles += emitter->GetParticles().size();
			}
		}
	}
	void UpdateEntities(std::map<GLuint, std::list<std::shared_ptr<Entity>>> t_entities) {
		//TODO: very slow
		//for each (std::pair<GLuint, std::list<std::shared_ptr<Entity>>> pair in t_entities) {
		//	for each(std::shared_ptr<Entity> entity in pair.second) entity->Update();
		//}
	}
	void UpdateGUIs(std::map<GLuint, std::vector<std::shared_ptr<GUI>>> t_GUIs) {
		for each(std::pair<GLuint, std::vector<std::shared_ptr<GUI>>> guiLists in t_GUIs) {
			for each(std::shared_ptr<GUI> gui in guiLists.second) {
				gui->Update();
			}
		}
	}
	void UpdateShadows(const std::list<std::shared_ptr<Terrain>>& t_terrains, const std::map<GLuint, std::list<std::shared_ptr<Entity>>>& t_entities, const Light& t_sun) {
		m_shadowMapMaster.Render(t_entities, t_terrains, t_sun, *m_camera);
	}
	void UpdateWater(std::list<std::shared_ptr<Terrain>>& t_terrains, std::shared_ptr<Skybox>& t_skybox, 
		std::map<GLuint, std::list<std::shared_ptr<Entity>>>& t_entities, 
		std::map<GLuint, std::list<std::shared_ptr<ParticleEmitter>>>& t_particleEmitters, 
		const std::vector<Light>& t_lights) {
		EnableClipping();

		//update reflection
		m_waterFBO.BindReflectionFrameBuffer();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//no idead why it has to be -WATER_HEIGHT and not WATER_HEIGHT, 
		//I remember some weirdness with the D attribute from math class, but I don't know
		InvertCamera();
		//0.1f offset to remove artifacts at edge of water
		m_masterRenderer.LoadClipPlane(Vec4(0.0f, 1.0f, 0.0f, -WATER_HEIGHT + 0.1f));
		m_masterRenderer.Render(t_terrains, t_skybox, t_entities, t_lights, *m_camera, m_shadowMapMaster.GetToShadowMapSpaceMatrix(), m_shadowMapMaster.GetShadowMap(), m_window.GetTime());
		m_particleMaster.Render(t_particleEmitters, *m_camera);
		InvertCamera();

		//update refraction
		m_waterFBO.BindRefractionFrameBuffer();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//1.0f offset to remove artifacts at edge of water
		m_masterRenderer.LoadClipPlane(Vec4(0.0f, -1.0f, 0.0f, WATER_HEIGHT + 1.0f));
		m_masterRenderer.Render(t_terrains, t_skybox, t_entities, t_lights, *m_camera, m_shadowMapMaster.GetToShadowMapSpaceMatrix(), m_shadowMapMaster.GetShadowMap(), m_window.GetTime());
		m_particleMaster.Render(t_particleEmitters, *m_camera);

		m_waterFBO.UnbindCurrentFrameBuffer(m_window);
		DisableClipping();
	}
	void UpdateMetadata() {		
		//setup metadata info
		m_metadataMap.clear();

		if (Input::GetKeyPress(GLFW_KEY_U)) {
			m_mouseUnlock = !m_mouseUnlock;
			if (m_mouseUnlock) {
				m_window.m_resetCursor = false;
				glfwSetInputMode(m_window.GetWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
			} else {
				m_window.m_resetCursor = true;
				glfwSetInputMode(m_window.GetWindow(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
			}
		}
		if (Input::GetKeyPress(GLFW_KEY_F3)) {
			m_displayInfo = !m_displayInfo;
		}

		if (!m_displayInfo) return;

		Vec3 currentRay = MousePickerPerspective::GetCurrentRay();
		std::string mouseInfo = "Vec3: " + StringManipulation::FloatToString(currentRay.x) + ", " + StringManipulation::FloatToString(currentRay.y) + ", " + StringManipulation::FloatToString(currentRay.z);
		m_textMousePerspective.SetText(mouseInfo, font);
		m_textMousePerspective.SetText("Number of vertices: " + StringManipulation::FloatToString((float)GameObjects::GetVertexCount()), font);

		std::string screenInfo = "Screen width: " + StringManipulation::FloatToString((float)m_window.m_width) + " Screen Height: " + StringManipulation::FloatToString((float)m_window.m_height) + " FPS: " + StringManipulation::FloatToString((float)m_fps.GetFrames());
		m_screenInfo.SetText(screenInfo, font);

		Vec3 camPos = m_camera->GetPosition();
		Vec3 camView = m_camera->GetViewDirection();
		std::string camPosText = "Camera position: " + StringManipulation::FloatToString(camPos.x) + ", " + StringManipulation::FloatToString(camPos.y) + ", " + StringManipulation::FloatToString(camPos.z);
		std::string camViewText = "Camera view direction: " + StringManipulation::FloatToString(camView.x) + ", " + StringManipulation::FloatToString(camView.y) + ", " + StringManipulation::FloatToString(camView.z);

		m_camPos.SetText(camPosText, font);
		m_camView.SetText(camViewText, font);

		std::string virtualMemTotal = "Total virtual memory: " + StringManipulation::FloatToString(m_process.m_totalVirtualMemory) + "GB";
		std::string virtualMemTotalUsed = "Virtual memory used: " + StringManipulation::FloatToString(m_process.m_virtualMemoryUsed) + "GB";
		std::string virtualMemProcess = "Virtual memory used by process: " + StringManipulation::FloatToString(m_process.m_virtualMemoryUsedProcess) + "GB";

		m_textVirtualMemTotal.SetText(virtualMemTotal, font);
		m_textVirtualMemUsed.SetText(virtualMemTotalUsed, font);
		m_textVirtualMemProcess.SetText(virtualMemProcess, font);

		std::string physicalMemTotal = "Total physical memory: " + StringManipulation::FloatToString(m_process.m_totalPhysicalMemory) + "GB";
		std::string physicalMemTotalUsed = "Physical memory used: " + StringManipulation::FloatToString(m_process.m_physicalMemoryUsed) + "GB";
		std::string physicalMemProcess = "Physical memory used by process: " + StringManipulation::FloatToString(m_process.m_phyisicalMemoryUsedProcess) + "GB";

		m_textPhysicalMemTotal.SetText(physicalMemTotal, font);
		m_textPhysicalMemUsed.SetText(physicalMemTotalUsed, font);
		m_textPhysicalMemProcess.SetText(physicalMemProcess, font);

		std::string cpuTotal = "Total CPU used: " + StringManipulation::FloatToString((float)m_process.m_CPUTotalUsed);
		std::string cpuProcess = "CPU used by process: " + StringManipulation::FloatToString((float)m_process.m_CPUProcessUsed);
		m_textCPUTotal.SetText(cpuTotal, font);
		m_textCPUProcess.SetText(cpuProcess, font);

		m_particlesOnScreen.SetText("Number of particles: " + StringManipulation::FloatToString((float)m_numberOfParticles), font);

		//render metadata
		if (m_displayInfo) {
			std::vector<std::shared_ptr<Text>> metadata = std::vector<std::shared_ptr<Text>>();
			metadata.push_back(std::make_shared<Text>(m_screenInfo));
			metadata.push_back(std::make_shared<Text>(m_camPos));
			//metadata.push_back(std::make_shared<Text>(m_camView));
			metadata.push_back(std::make_shared<Text>(m_textMousePerspective));
			//metadata.push_back(std::make_shared<Text>(m_lookText));
			//metadata.push_back(std::make_shared<Text>(m_textVirtualMemTotal));
			//metadata.push_back(std::make_shared<Text>(m_textVirtualMemUsed));
			//metadata.push_back(std::make_shared<Text>(m_textVirtualMemProcess));
			//metadata.push_back(std::make_shared<Text>(m_textPhysicalMemTotal));
			//metadata.push_back(std::make_shared<Text>(m_textPhysicalMemUsed));
			//metadata.push_back(std::make_shared<Text>(m_textPhysicalMemProcess));
			//metadata.push_back(std::make_shared<Text>(m_textCPUTotal));
			//metadata.push_back(std::make_shared<Text>(m_textCPUProcess));
			metadata.push_back(std::make_shared<Text>(m_particlesOnScreen));
			
			m_metadataMap.insert(std::pair<GLuint, std::vector<std::shared_ptr<Text>>>(font.GetTextureID(), metadata));
		}
	}
};